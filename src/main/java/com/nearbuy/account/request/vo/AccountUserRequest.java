package com.nearbuy.account.request.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.nearbuy.account.response.vo.AccountNameVO;

public class AccountUserRequest {

    @NotEmpty
    private String name;

    @NotEmpty
    private String email;

    @NotEmpty
    private String role;

    // private List<PhoneNumber> phoneNumbers;

    // @NotEmpty
    // private String accountId;

    private AccountNameVO account;

    private String linkedInUrl;

    private String designation;

    // private String createdBy;

    // private String updatedBy;

    private String stdCode;

    private String landline;

    private String mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    // public String getCreatedBy() {
    // return createdBy;
    // }
    //
    // public void setCreatedBy(String createdBy) {
    // this.createdBy = createdBy;
    // }
    //
    // public String getUpdatedBy() {
    // return updatedBy;
    // }
    //
    // public void setUpdatedBy(String updatedBy) {
    // this.updatedBy = updatedBy;
    // }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "AccountUserRequest [name=" + name + ", email=" + email + ", role=" + role + ", linkedInUrl=" + linkedInUrl + ", designation=" + designation
                + ", stdCode=" + stdCode + ", landline=" + landline + ", mobile=" + mobile + "]";
    }

    public AccountNameVO getAccount() {
        return account;
    }

    public void setAccount(AccountNameVO account) {
        this.account = account;
    }

}
