package com.nearbuy.account.request.vo;

public class BankAccountRequest {

    /*
     * BankDetail - name,ifsc, a/c number, A/c holder's name, nickname,
     */
    private String name;

    private String ifsc;

    private String acNumber;

    private String acHolderName;

    private String nickname;

    private String chequeId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getAcNumber() {
        return acNumber;
    }

    public void setAcNumber(String acNumber) {
        this.acNumber = acNumber;
    }

    public String getAcHolderName() {
        return acHolderName;
    }

    public void setAcHolderName(String acHolderName) {
        this.acHolderName = acHolderName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getChequeId() {
        return chequeId;
    }

    public void setChequeId(String chequeId) {
        this.chequeId = chequeId;
    }

    @Override
    public String toString() {
        return "BankAccountRequest [name=" + name + ", ifsc=" + ifsc + ", acNumber=" + acNumber + ", acHolderName=" + acHolderName + ", nickname=" + nickname
                + ", chequeId=" + chequeId + "]";
    }

}
