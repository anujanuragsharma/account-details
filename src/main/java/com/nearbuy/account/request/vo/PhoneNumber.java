package com.nearbuy.account.request.vo;

public class PhoneNumber {
    private String phoneNumber;

    private String phoneType;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public PhoneNumber(String phoneType, String phoneNumber) {
        this.phoneNumber = phoneNumber;
        this.phoneType = phoneType;
    }

    public PhoneNumber() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return "PhoneNumber [phoneNumber=" + phoneNumber + ", phoneType=" + phoneType + "]";
    }
}
