package com.nearbuy.account.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.account.exception.UserException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.ResetPasswordRequest;
import com.nearbuy.account.request.vo.AccountUserRequest;
import com.nearbuy.account.response.vo.AccountUserResponse;
import com.nearbuy.account.response.vo.AccountUserVO;
import com.nearbuy.account.response.vo.ErrorResponseVo;
import com.nearbuy.account.response.vo.User;
import com.nearbuy.account.service.AccountUserService;

@RestController
@RequestMapping("/api/accounts/{accountId}/users")
public class BusinessAccountUserController {

    private Logger logger = LoggerFactory.getLogger(BusinessAccountUserController.class);

    // @Autowired
    // private BusinessAccountFacadeImpl businessAccFacade;

    @Autowired
    private AccountUserService accountUserService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Object> createAccountUser(@PathVariable String accountId, @RequestBody @Valid AccountUserRequest accountUserRequest,
            HttpServletRequest request) {
        logger.info("Entered in BusinessAccountUserController.createAccountUser");
        User user = (User) request.getAttribute("user");
        AccountUserResponse response = null;

        try {
            response = accountUserService.createAccountUser(accountId, accountUserRequest, user);
        } catch (UserException e) {
            logger.error("Exception in creating Account user, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exited from BusinessAccountUserController.createAccountUser with response " + response);
        ResponseEntity<Object> entity = new ResponseEntity<Object>(response, HttpStatus.OK);
        return entity;

    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateAccountUser(@PathVariable String accountId, @RequestBody @Valid AccountUserRequest updateRequest,
            @PathVariable String userId,
            HttpServletRequest request) {
        logger.info("Entered in BusinessAccountUserController.updateAccountUser");
        User user = (User) request.getAttribute("user");
        AccountUserResponse response = null;

        try {
            response = accountUserService.updateAccountUser(accountId, updateRequest, userId, user);
        } catch (UserException e) {
            logger.error("Exception in updating Account user, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exited from BusinessAccountUserController.updateAccountUser with response " + response);
        ResponseEntity<Object> entity = new ResponseEntity<Object>(new BaseResponse(response.getMsg()), HttpStatus.OK);
        return entity;

    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getAccountUserDetails(@PathVariable String accountId, @PathVariable String userId) {
        logger.info("Entered in BusinessAccountUserController.getAccountUserDetails");
        AccountUserVO dto = null;
        try {
            dto = accountUserService.getAccountUserByUserId(accountId, userId);
        } catch (UserException e) {
            logger.error("Exception in fetching Account user, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountUserController.getAccountUserDetails with response " + dto);
        ResponseEntity<Object> resp = new ResponseEntity<>(dto, HttpStatus.OK);
        return resp;
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteAccountUser(@PathVariable String accountId, @PathVariable String userId) {
        logger.info("Entered in BusinessAccountUserController.deleteAccountUser");
        AccountUserResponse response = null;
        try {
            response = accountUserService.deleteAccountUser(accountId, userId);
        } catch (UserException e) {
            logger.error("Exception in deleting Account user, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountUserController.deleteAccountUser with response " + response);
        ResponseEntity<Object> entity = new ResponseEntity<Object>(new BaseResponse(response.getMsg()), HttpStatus.OK);
        return entity;
    }

    @RequestMapping(value = "/{userId}/resetpassword", method = RequestMethod.POST)
    public ResponseEntity<Object> resetPasswordForAccountUser(@PathVariable String accountId, @PathVariable String userId,
            @RequestBody(required = false) ResetPasswordRequest resetRequest, HttpServletRequest request) {
        logger.info("Entered in BusinessAccountUserController.resetPasswordForAccountUser");
        AccountUserResponse response = null;
        User user = (User) request.getAttribute("user");
        try {
            response = accountUserService.resetPasswordForAccountUser(accountId, userId, resetRequest, user);
        } catch (UserException e) {
            logger.error("Exception in resetting password for Account user, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountUserController.resetPasswordForAccountUser with response ");
        ResponseEntity<Object> entity = new ResponseEntity<Object>(new BaseResponse(response.getMsg()), HttpStatus.OK);
        return entity;
    }

}
