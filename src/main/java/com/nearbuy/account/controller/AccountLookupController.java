package com.nearbuy.account.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.account.model.AccountLookupResponse;
import com.nearbuy.account.service.BusinessAccountService;

@RestController
public class AccountLookupController {

    @Autowired
    private BusinessAccountService accountService;

    private Logger logger = LoggerFactory.getLogger(AccountLookupController.class);

    @RequestMapping(value = "/api/accounts/lookup/{search}", method = RequestMethod.GET)
    public List<AccountLookupResponse> lookupAccounts(@PathVariable String search) {
        logger.info("Entered AccountLookupController.lookupMerchant with request : " + search);
        List<AccountLookupResponse> response = accountService.lookupAccount(search);
        logger.info("Exiting AccountLookupController.lookupMerchant with response : " + response);
        return response;
    }

}
