package com.nearbuy.account.controller;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nearbuy.account.exception.ImageNotFoundException;
import com.nearbuy.account.exception.UploadException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.ImageID;
import com.nearbuy.account.model.ImageResponse;
import com.nearbuy.account.service.ImageService;

@RestController
public class ImageUploadController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/api/image/upload", method = RequestMethod.POST)
    public ResponseEntity<Object> uploadImage(@RequestParam(value = "file", required = true) MultipartFile file) {
        ResponseEntity<Object> entity = null;
        ImageID msg = null;
        try {
            msg = imageService.uploadImage(file);
        } catch (UploadException e) {
            // Log this
            e.printStackTrace();
            entity = new ResponseEntity<>(new BaseResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        entity = new ResponseEntity<>(msg, HttpStatus.OK);
        return entity;
    }

    @RequestMapping(value = "/api/image/fetch/{id}", method = RequestMethod.GET)
    public void fetchImage(@PathVariable String id, HttpServletResponse response) {
        ImageResponse image = null;
        try {
            image = imageService.fetchSecuredImage(id, true);
            if (null != image) {
                ServletOutputStream stream = response.getOutputStream();

                String headerKey = "Content-Disposition";
                response.setHeader(headerKey, image.getContentDisposition());
                MediaType mimeType = (null == image.getContentType() ? MediaType.APPLICATION_OCTET_STREAM : image.getContentType());
                response.setContentType(mimeType.toString());
                stream.write(image.getImage());
                stream.close();
            }

        } catch (ImageNotFoundException e) {
            // Log this
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/api/image/download/{id}", method = RequestMethod.GET)
    public void downloadImage(@PathVariable String id, HttpServletResponse response) {
        ImageResponse image = null;
        try {
            image = imageService.fetchSecuredImage(id, false);
            if (null != image) {
                ServletOutputStream stream = response.getOutputStream();

                String headerKey = "Content-Disposition";
                response.setHeader(headerKey, image.getContentDisposition());
                MediaType mimeType = (null == image.getContentType() ? MediaType.APPLICATION_OCTET_STREAM : image.getContentType());
                response.setContentType(mimeType.toString());
                stream.write(image.getImage());
                stream.close();
            }

        } catch (ImageNotFoundException e) {
            // Log this
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
