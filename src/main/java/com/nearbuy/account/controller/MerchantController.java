package com.nearbuy.account.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.facade.MerchantDetailsFacadeImpl;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.ErrorResponseVo;
import com.nearbuy.account.response.vo.MerchantDetailsVO;

@RestController
@RequestMapping("/api/merchants/{merchantId}")
public class MerchantController {

    @Autowired
    private MerchantDetailsFacadeImpl merchantFacade;

    private Logger logger = LoggerFactory.getLogger(MerchantController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public MerchantDetailsVO getMerchantDetails(@PathVariable String merchantId) {
        logger.info("Entered in MerchantController.getMerchantDetails");
        MerchantDetailsVO vo = merchantFacade.getMerchantDetails(merchantId);
        logger.info("Exiting from MerchantController.getMerchantDetails with response " + vo);
        return vo;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateMerchantDetails(@PathVariable String merchantId, @RequestBody @Valid MerchantUpdateRequest updateRequest) {
        logger.info("Entered in MerchantController.updateMerchantDetails");
        BaseResponse response = null;
        try {
            response = merchantFacade.updateMerchant(merchantId, updateRequest);
        } catch (MerchantException e) {
            logger.error("Exception in updating Merchant, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from MerchantController.updateMerchantDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

}
