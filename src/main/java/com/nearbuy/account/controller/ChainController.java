package com.nearbuy.account.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.facade.ChainDetailsFacadeImpl;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.ChainDetailsVO;
import com.nearbuy.account.response.vo.ErrorResponseVo;

@RestController
@RequestMapping("/api/chains/{chainId}")
public class ChainController {

    @Autowired
    private ChainDetailsFacadeImpl chainFacade;

    private Logger logger = LoggerFactory.getLogger(ChainController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ChainDetailsVO getChainDetails(@PathVariable String chainId) {
        logger.info("Entered in ChainController.getChainDetails");
        ChainDetailsVO vo = chainFacade.getChainDetails(chainId);
        logger.info("Exiting from ChainController.getChainDetails with response " + vo);
        return vo;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateChainDetails(@PathVariable String chainId, @RequestBody @Valid MerchantUpdateRequest updateRequest) {
        logger.info("Entered in ChainController.updateChainDetails");
        BaseResponse response = null;
        try {
            response = chainFacade.updateChain(chainId, updateRequest);
        } catch (MerchantException e) {
            logger.error("Exception in updating Business Account, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountController.updateAccountDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

}
