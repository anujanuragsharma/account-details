package com.nearbuy.account.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.account.exception.BusinessAccountException;
import com.nearbuy.account.facade.BusinessAccountFacadeImpl;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.request.vo.BankAccountRequest;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.BusinessAccountVO;
import com.nearbuy.account.response.vo.ErrorResponseVo;

@RestController
@RequestMapping("/api/accounts/{accountId}")
public class BusinessAccountController {

    private Logger logger = LoggerFactory.getLogger(BusinessAccountController.class);

    @Autowired
    private BusinessAccountFacadeImpl businessAccFacade;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Object> getAccountDetails(@PathVariable String accountId) {
        logger.info("Entered in BusinessAccountController.getAccountDetails");
        BusinessAccountVO vo;
        try {
            vo = businessAccFacade.getBusinessAccountDetails(accountId);
        } catch (BusinessAccountException e) {
            logger.error("Exception in updating Business Account, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountController.getAccountDetails with response " + vo);
        return new ResponseEntity<Object>(vo, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateAccountDetails(@PathVariable String accountId, @RequestBody @Valid MerchantUpdateRequest updateRequest) {
        logger.info("Entered in BusinessAccountController.updateAccountDetails");
        BaseResponse response = null;
        try {
            response = businessAccFacade.updateBusinessAccount(accountId, updateRequest);
        } catch (BusinessAccountException e) {
            logger.error("Exception in updating Business Account, Exception " + e);
            ResponseEntity<Object> entity = new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
            return entity;
        }
        logger.info("Exiting from BusinessAccountController.updateAccountDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/bankDetails", method = RequestMethod.POST)
    public ResponseEntity<Object> createBankDetails(@PathVariable String accountId, @RequestBody BankAccountRequest request) {
        logger.info("Entered in BusinessAccountController.createBankDetails");
        // TODO ARIA-98
        BaseResponse response = null;
        try {
            response = businessAccFacade.createBankDetailsForAccount(accountId, request);
        } catch (BusinessAccountException e) {
            logger.error("Exception in creting Bank details for Account" + accountId + ", Exception " + e);
            return new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        logger.info("Exiting from BusinessAccountController.createBankDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/bankDetails/{bankAcctId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateBankDetails(@PathVariable String accountId, @PathVariable Long bankAcctId, @RequestBody BankAccountRequest request) {
        logger.info("Entered in BusinessAccountController.updateBankDetails");
        BaseResponse response = null;
        try {
            response = businessAccFacade.updateBankDetailsForAccount(accountId, bankAcctId, request);
        } catch (BusinessAccountException e) {
            logger.error("Exception in creting Bank details for Account" + accountId + ", Exception " + e);
            return new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        logger.info("Exiting from BusinessAccountController.createBankDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/bankDetails/{bankAcctId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteBankDetails(@PathVariable String accountId, @PathVariable String bankAcctId) {
        logger.info("Entered in BusinessAccountController.deleteBankDetails");
        // TODO JIRA-98
        BaseResponse response = null;
        try {
            response = businessAccFacade.deleteBankDetailsForAccount(accountId, bankAcctId);
        } catch (BusinessAccountException e) {
            logger.error("Exception in deleting Bank details for Account" + accountId + ", Exception " + e);
            return new ResponseEntity<Object>(new ErrorResponseVo(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        logger.info("Exiting from BusinessAccountController.deleteBankDetails with response " + response);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

}
