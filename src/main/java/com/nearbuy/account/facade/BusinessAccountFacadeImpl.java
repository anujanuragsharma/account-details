package com.nearbuy.account.facade;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.nearbuy.account.exception.BusinessAccountException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.BusinessAccountDTO;
import com.nearbuy.account.model.MerchantPhoneNumber;
import com.nearbuy.account.request.vo.BankAccountRequest;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.BusinessAccountVO;
import com.nearbuy.account.service.BusinessAccountService;
import com.nearbuy.account.service.MerchantService;

@Service
public class BusinessAccountFacadeImpl {

    // @Autowired
    // private AccountUserService accountUserService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private BusinessAccountService businessAccountService;

    private Logger logger = LoggerFactory.getLogger(BusinessAccountFacadeImpl.class);

    public BusinessAccountVO getBusinessAccountDetails(String businessAccId) throws BusinessAccountException {
        logger.info("Entered in BusinessAccountFacadeImpl.getBusinessAccountDetails with request " + businessAccId);

        // fetch account details
        BusinessAccountVO accountVO = businessAccountService.getDetailsForBusinessAccount(businessAccId);
        if (null == accountVO) {
            logger.error("Business Account with id " + businessAccId + " not found");
            return accountVO;
        }

        // TODO fetch merchant details

        List<AssociatedMerchantDetail> merchants = merchantService.getMerchantsForAccountId(Long.valueOf(businessAccId));
        accountVO.setMerchants(merchants);
        /*
         * // fetch account users
         * List<AccountUserVO> accUsers = accountUserService.getAccountUsersForBusinessAccount(businessAccId);
         * 
         * accountVO.setAccountUsers(accUsers);
         */

        logger.info("Exiting BusinessAccountFacadeImpl.getBusinessAccountDetails with response " + accountVO);
        return accountVO;
    }

    public BaseResponse updateBusinessAccount(String accountId, MerchantUpdateRequest updateRequest) throws BusinessAccountException {
        BusinessAccountDTO dto = new BusinessAccountDTO();
        boolean updateFlag = false;
        String value = updateRequest.getValue();
        if ("legalName".equals(updateRequest.getKey())) {
            dto.setLegalName(value);
            updateFlag = true;
        }

        if ("phoneNumber".equals(updateRequest.getKey())) {
            // String phone = transformToPhoneString(value);
            dto.setPhoneNumber(value);
            updateFlag = true;
        }

        if ("emailAddress".equals(updateRequest.getKey())) {
            dto.setEmailAddress(value);
            updateFlag = true;
        }

        if (!updateFlag) {
            throw new BusinessAccountException("Only legalName, phoneNumber and emailAddress can be updated");
        }

        BaseResponse accountVO = businessAccountService.updateBusinessAccount(accountId, dto);

        return accountVO;
    }

    // private String transformToPhoneString(String value) {
    // MerchantPhoneNumber number = new Gson().fromJson(value, MerchantPhoneNumber.class);
    // if (null != number) {
    // String phone = number.getCountryCode().concat(number.getValue());
    // return phone;
    // } else {
    // return null;
    // }
    // }

    public BaseResponse createBankDetailsForAccount(String accountId, BankAccountRequest request) throws BusinessAccountException {

        BaseResponse response = businessAccountService.createBankDetailsForAccount(accountId, request);

        return response;
    }

    public BaseResponse deleteBankDetailsForAccount(String accountId, String bankAccountId) throws BusinessAccountException {

        BaseResponse response = businessAccountService.deleteBankDetailsForAccount(accountId, bankAccountId);

        return response;
    }

    public BaseResponse updateBankDetailsForAccount(String accountId, Long bankAcctId, BankAccountRequest request) throws BusinessAccountException {
        BaseResponse response = businessAccountService.updateBankDetailsForAccount(accountId, bankAcctId, request);

        return response;

    }

}
