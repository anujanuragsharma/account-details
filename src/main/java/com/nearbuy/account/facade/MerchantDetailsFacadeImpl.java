package com.nearbuy.account.facade;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantPhoneNumber;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.MerchantDetailsVO;
import com.nearbuy.account.service.MerchantService;

@Service
public class MerchantDetailsFacadeImpl {

    private Logger logger = LoggerFactory.getLogger(MerchantDetailsFacadeImpl.class);

    @Autowired
    private MerchantService merchantService;

    public MerchantDetailsVO getMerchantDetails(String merchantId) {

        logger.info("Entered in MerchantDetailsFacadeImpl.getMerchantDetails with request " + merchantId);

        // fetch merchant details
        MerchantDetailsVO merchantVO = merchantService.getDetailsForMerchant(merchantId);

        if (null == merchantVO) {
            logger.error("Merchant with id " + merchantId + " not found");
            return merchantVO;
        }

        logger.info("Exiting MerchantDetailsFacadeImpl.getMerchantDetails with response " + merchantVO);
        return merchantVO;

    }

    public BaseResponse updateMerchant(String merchantId, MerchantUpdateRequest updateRequest) throws MerchantException {
        MerchantDTO dto = new MerchantDTO();
        BaseResponse response = null;
        if ("emailId".equals(updateRequest.getKey())) {
            List<String> value = (List) new Gson().fromJson(updateRequest.getValue(), List.class);
            dto.setContactEmailId(value);
            response = merchantService.updateMerchant(merchantId, dto);
            return response;
        } else if ("reservationEmailId".equals(updateRequest.getKey())) {
            List<String> value = (List) new Gson().fromJson(updateRequest.getValue(), List.class);
            dto.setReservationEmailId(value);
            response = merchantService.updateMerchant(merchantId, dto);
            return response;
        } else if ("phoneNumbers".equals(updateRequest.getKey())) {
            MerchantPhoneNumber number = new Gson().fromJson(updateRequest.getValue(), MerchantPhoneNumber.class);
            PhoneDetailDTO phDTO = new PhoneDetailDTO();
            phDTO.setId(number.getId());
            phDTO.setContactNo(number.getValue());
            phDTO.setCountryCode(number.getCountryCode());
            phDTO.setStdCode(number.getStdCode());
            phDTO.setType(number.getType());
            response = merchantService.updateMerchantPhoneNumber(merchantId, String.valueOf(number.getId()), phDTO);
            return response;
        } else if ("reservationNumber".equals(updateRequest.getKey())) {
            MerchantPhoneNumber number = new Gson().fromJson(updateRequest.getValue(), MerchantPhoneNumber.class);
            PhoneDetailDTO phDTO = new PhoneDetailDTO();
            phDTO.setId(number.getId());
            phDTO.setContactNo(number.getValue());
            phDTO.setCountryCode(number.getCountryCode());
            phDTO.setStdCode(number.getStdCode());
            phDTO.setType(number.getType());
            response = merchantService.updateMerchantReservationNumber(merchantId, String.valueOf(number.getId()), phDTO);
            return response;
        }
        return new BaseResponse("Update is not supported for the field : " + updateRequest.getKey());
    }

}
