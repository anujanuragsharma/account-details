package com.nearbuy.account.facade;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantPhoneNumber;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.request.vo.MerchantUpdateRequest;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.ChainDetailsVO;
import com.nearbuy.account.service.ChainService;

@Service
public class ChainDetailsFacadeImpl {

    private Logger logger = LoggerFactory.getLogger(ChainDetailsFacadeImpl.class);

    @Autowired
    private ChainService chainService;

    public ChainDetailsVO getChainDetails(String chainId) {

        logger.info("Entered in ChainDetailsFacadeImpl.getChainDetails with request " + chainId);

        // fetch merchant details
        ChainDetailsVO chainVO = chainService.getDetailsForChain(chainId);

        if (null == chainVO) {
            logger.error("Merchant with id " + chainId + " not found");
            return chainVO;
        }

        List<AssociatedMerchantDetail> merchants = chainService.getAssociatedMerchantsForChain(chainId);

        chainVO.setAssociatedMerchants(merchants);

        logger.info("Exiting ChainDetailsFacadeImpl.getChainDetails with response " + chainVO);
        return chainVO;

    }

    public BaseResponse updateChain(String chainId, MerchantUpdateRequest updateRequest) throws MerchantException {
        MerchantDTO dto = new MerchantDTO();
        BaseResponse response = null;
        if ("emailIds".equals(updateRequest.getKey())) {
            List value = new Gson().fromJson(updateRequest.getValue(), List.class);
            dto.setContactEmailId(value);
            response = chainService.updateChain(chainId, dto);
        }
        else if("phoneNumbers".equals(updateRequest.getKey())){
            MerchantPhoneNumber number = new Gson().fromJson(updateRequest.getValue(), MerchantPhoneNumber.class);
            PhoneDetailDTO phDTO = new PhoneDetailDTO();
            phDTO.setId(number.getId());
            phDTO.setContactNo(number.getValue());
            phDTO.setCountryCode(number.getCountryCode());
            phDTO.setStdCode(number.getStdCode());
            phDTO.setType(number.getType());
            response = chainService.updateChainPhoneNumber(chainId, String.valueOf(number.getId()), phDTO);
        }
        
        return response;
    }

}
