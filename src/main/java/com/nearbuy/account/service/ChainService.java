package com.nearbuy.account.service;

import java.util.List;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.ChainDetailsVO;

public interface ChainService {

    ChainDetailsVO getDetailsForChain(String chainId);

    List<AssociatedMerchantDetail> getAssociatedMerchantsForChain(String chainId);

    BaseResponse updateChain(String chainId, MerchantDTO chain) throws MerchantException;

    BaseResponse updateChainPhoneNumber(String chainId, String valueOf, PhoneDetailDTO phDTO) throws MerchantException;

}
