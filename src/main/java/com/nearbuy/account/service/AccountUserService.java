package com.nearbuy.account.service;

import java.util.List;

import com.nearbuy.account.exception.UserException;
import com.nearbuy.account.model.ResetPasswordRequest;
import com.nearbuy.account.model.UserAccIdDetail;
import com.nearbuy.account.request.vo.AccountUserRequest;
import com.nearbuy.account.response.vo.AccountUserResponse;
import com.nearbuy.account.response.vo.AccountUserVO;
import com.nearbuy.account.response.vo.User;

public interface AccountUserService {

    List<AccountUserVO> getAccountUsersForBusinessAccount(String businessAccId);

    AccountUserVO getAccountUserByUserId(String businessAccId, String userId) throws UserException;

    AccountUserResponse createAccountUser(String accountId, AccountUserRequest accountUserRequest, User user) throws UserException;

    AccountUserResponse updateAccountUser(String accountId, AccountUserRequest updateRequest, String userId, User user) throws UserException;

    AccountUserResponse deleteAccountUser(String accountId, String userId) throws UserException;

    AccountUserResponse resetPasswordForAccountUser(String accountId, String userId, ResetPasswordRequest resetRequest, User user) throws UserException;

    UserAccIdDetail getAccountUserByEmailId(String emailId) throws UserException;
    

}
