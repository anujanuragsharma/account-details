package com.nearbuy.account.service;

import java.util.List;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.MerchantDetailsVO;

public interface MerchantService {

    MerchantDetailsVO getDetailsForMerchant(String merchantId);

    BaseResponse updateMerchant(String merchantId, MerchantDTO merchant) throws MerchantException;

    BaseResponse updateMerchantPhoneNumber(String merchantId, String phoneId, PhoneDetailDTO detailDTO) throws MerchantException;

    BaseResponse updateMerchantReservationNumber(String merchantId, String phoneId, PhoneDetailDTO detailDTO) throws MerchantException;

    List<AssociatedMerchantDetail> getMerchantsForAccountId(Long accountId);

}
