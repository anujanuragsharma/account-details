package com.nearbuy.account.service;

import org.springframework.web.multipart.MultipartFile;

import com.nearbuy.account.exception.ImageNotFoundException;
import com.nearbuy.account.exception.UploadException;
import com.nearbuy.account.model.ImageDTO;
import com.nearbuy.account.model.ImageID;
import com.nearbuy.account.model.ImageResponse;

public interface ImageService {

    public ImageDTO fetchImageDetailsFromId(String id);

    public ImageID uploadImage(MultipartFile file) throws UploadException;

    public ImageResponse fetchSecuredImage(String id, boolean onlyView) throws ImageNotFoundException;

}
