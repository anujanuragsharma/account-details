package com.nearbuy.account.service;

import java.util.List;

import com.nearbuy.account.exception.BusinessAccountException;
import com.nearbuy.account.model.AccountLookupResponse;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.BusinessAccountDTO;
import com.nearbuy.account.request.vo.BankAccountRequest;
import com.nearbuy.account.response.vo.BusinessAccountVO;

public interface BusinessAccountService {

    BusinessAccountVO getDetailsForBusinessAccount(String businessAccId) throws BusinessAccountException;

    BaseResponse updateBusinessAccount(String accountId, BusinessAccountDTO accountDTO) throws BusinessAccountException;

    BaseResponse createBankDetailsForAccount(String accountId, BankAccountRequest detail) throws BusinessAccountException;

    BaseResponse deleteBankDetailsForAccount(String accountId, String bankAccountId) throws BusinessAccountException;
    
    List<AccountLookupResponse> lookupAccount(String search);

    BaseResponse updateBankDetailsForAccount(String accountId, Long bankAcctId, BankAccountRequest request) throws BusinessAccountException;

}
