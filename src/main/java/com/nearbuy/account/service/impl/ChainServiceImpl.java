package com.nearbuy.account.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.ImageDTO;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantServiceResponse;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.ChainDetailsVO;
import com.nearbuy.account.service.ChainService;
import com.nearbuy.account.service.ImageService;
import com.nearbuy.account.util.MapperUtil;

@Service
public class ChainServiceImpl implements ChainService {

    private Logger logger = LoggerFactory.getLogger(ChainServiceImpl.class);

    @Value("${merchantService.merchantDetails}")
    private String merchantDetailsEndpoint;

    @Value("${merchantService.chainAssociatedMerchants}")
    private String chainAssociatedMerchantsEndpoint;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Value("${merchantService.merchantUpdate}")
    private String chainUpdateEndpoint;

    @Value("${merchantService.merchantPhoneUpdate}")
    private String chainPhoneUpdateEndpoint;

    @Autowired
    private ImageService imageService;

    @Override
    public ChainDetailsVO getDetailsForChain(String chainId) {
        logger.info("Entered ChainServiceImpl.getDetailsForChain for request " + chainId);
        String uri = merchantDetailsEndpoint + chainId;
        ResponseEntity<MerchantServiceResponse<MerchantDTO>> exchange = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO>>() {
                });
        MerchantServiceResponse<MerchantDTO> dto = exchange.getBody();
        MerchantDTO merchant = null;
        if (null != dto.getEr()) {
            logger.error("Error while fetching Chain details : " + dto.getEr());
        } else {
            merchant = (MerchantDTO) dto.getRs();
        }

        fetchAndSetChainImages(merchant);

        ChainDetailsVO chainDetailsVO = MapperUtil.convertToChainDetailViewObject(merchant);
        logger.info("Exiting ChainServiceImpl.getDetailsForChain with response " + chainDetailsVO);
        return chainDetailsVO;
    }

    private void fetchAndSetChainImages(MerchantDTO chain) {
        if (null == chain) {
            return;
        }
        // setting bannerImage
        ImageDTO banner = imageService.fetchImageDetailsFromId(chain.getBannerImage());
        if (null != banner) {
            String bannerUrl = banner.getUrl();
            chain.setBannerImage(bannerUrl);
        }

        // setting companylogo
        ImageDTO companyLogo = imageService.fetchImageDetailsFromId(chain.getCompanyLogoId());
        if (null != companyLogo) {
            String companyUrl = companyLogo.getUrl();
            chain.setCompanyLogoId(companyUrl);
        }

        // setting Category Images
        // TODO

    }

    @Override
    public List<AssociatedMerchantDetail> getAssociatedMerchantsForChain(String chainId) {
        logger.info("Entered ChainServiceImpl.getAssociatedMerchantsForChain for request " + chainId);
        String uri = chainAssociatedMerchantsEndpoint + chainId;
        ResponseEntity<MerchantServiceResponse<MerchantDTO[]>> exchange = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO[]>>() {
                });
        MerchantServiceResponse<MerchantDTO[]> dto = exchange.getBody();
        MerchantDTO[] merchants = null;
        if (null != dto.getEr()) {
            logger.error("Error while fetching Merchants for Chain : " + dto.getEr());
        } else {
            merchants = dto.getRs();
        }
        List<AssociatedMerchantDetail> associatedMerchants = new ArrayList<>();
        for (MerchantDTO merchant : merchants) {
            AssociatedMerchantDetail detail = MapperUtil.convertToAssociatedMerchantDetailViewObject(merchant);
            associatedMerchants.add(detail);
        }
        logger.info("Exiting ChainServiceImpl.getAssociatedMerchantsForChain with response " + associatedMerchants);
        return associatedMerchants;
    }

    @Override
    public BaseResponse updateChain(String chainId, MerchantDTO chain) throws MerchantException {
        logger.info(
                "Entered ChainServiceImpl.updateChain for account " + chainId + " with request : " + chain);
        String uri = chainUpdateEndpoint + chainId;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MerchantDTO> request = new HttpEntity<MerchantDTO>(chain, headers);
        ResponseEntity<MerchantServiceResponse<MerchantDTO>> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO>>() {
                });
        MerchantServiceResponse<MerchantDTO> dto = exchange.getBody();
        if (null != dto.getEr()) {
            logger.error("Error while updating Chain details : " + dto.getEr());
            throw new MerchantException(
                    (null != dto.getEr() ? dto.getEr().getMessage() : ("Error while updating chain with id " + chainId)));
        } else {
            logger.info("Chain with id " + chainId + " updated successfully");
            return new BaseResponse("Chain with id " + chainId + " updated successfully");
        }
    }

    @Override
    public BaseResponse updateChainPhoneNumber(String chainId, String phoneId, PhoneDetailDTO phDTO) throws MerchantException {
        logger.info(
                "Entered ChainServiceImpl.updateChainPhoneNumber for chain " + chainId + " with request : " + phDTO);
        String uri = MessageFormat.format(chainPhoneUpdateEndpoint, chainId, phoneId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PhoneDetailDTO> request = new HttpEntity<PhoneDetailDTO>(phDTO, headers);
        ResponseEntity<MerchantServiceResponse<PhoneDetailDTO>> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                new ParameterizedTypeReference<MerchantServiceResponse<PhoneDetailDTO>>() {
                });
        MerchantServiceResponse<PhoneDetailDTO> dto = exchange.getBody();
        if (null != dto.getEr()) {
            logger.error("Error while updating phone number with id " + phoneId + " for Chain with id " + chainId + ", Error: " + dto.getEr());
            throw new MerchantException(
                    (null != dto.getEr() ? dto.getEr().getMessage()
                            : ("Error while updating phone number with id " + phoneId + " for Chain with id " + chainId)));
        } else {
            logger.info("Phone number with id " + phoneId + " for chain " + chainId + " updated successfully");
            return new BaseResponse("Phone number with id " + phoneId + " for chain " + chainId + " updated successfully");
        }
    }

}
