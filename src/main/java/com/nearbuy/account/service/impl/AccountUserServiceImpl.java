package com.nearbuy.account.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.nearbuy.account.exception.InvalidRequestException;
import com.nearbuy.account.exception.UserException;
import com.nearbuy.account.model.AccountUserDTO;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.PhoneNumberDTO;
import com.nearbuy.account.model.ResetPasswordRequest;
import com.nearbuy.account.model.RoleDTO;
import com.nearbuy.account.model.UserAccIdDetail;
import com.nearbuy.account.model.UserRequest;
import com.nearbuy.account.request.vo.AccountUserRequest;
import com.nearbuy.account.request.vo.PhoneNumber;
import com.nearbuy.account.response.vo.AccountNameVO;
import com.nearbuy.account.response.vo.AccountUserResponse;
import com.nearbuy.account.response.vo.AccountUserVO;
import com.nearbuy.account.response.vo.ErrorResponseVo;
import com.nearbuy.account.response.vo.User;
import com.nearbuy.account.service.AccountUserService;

@Service
public class AccountUserServiceImpl implements AccountUserService {

    private Logger logger = LoggerFactory.getLogger(AccountUserServiceImpl.class);

    @Value("${accountService.userDetails}")
    private String accountUserDetailsEndpoint;

    @Value("${accountService.userCreate}")
    private String accountUserCreateEndpoint;

    @Value("${accountService.userUpdate}")
    private String accountUserUpdateEndpoint;

    @Value("${accountService.userDelete}")
    private String accountUserDeleteEndpoint;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Value("${accountService.user.reset.password}")
    private String resetPwdEndpoint;

    @Value("${accountService.user.email.details}")
    private String userDetailsByEmail;

    @Override
    public List<AccountUserVO> getAccountUsersForBusinessAccount(String businessAccId) {
        logger.info("Entered AccountUserServiceImpl.getAccountUsersForBusinessAccount with request " + businessAccId);
        String uri = MessageFormat.format(accountUserDetailsEndpoint, businessAccId);
        ResponseEntity<AccountUserDTO[]> entity = restTemplate.exchange(uri, HttpMethod.GET, null, AccountUserDTO[].class);
        AccountUserDTO[] list = entity.getBody();
        List<AccountUserVO> users = new ArrayList<AccountUserVO>();

        for (AccountUserDTO dto : list) {
            AccountUserVO user = new AccountUserVO();
            AccountNameVO vo = new AccountNameVO();
            vo.setId(Long.parseLong(dto.getAccountId()));
            vo.setName(dto.getAccountName());
            user.setAccount(vo);
            user.setDesignation(dto.getDesignation());
            user.setEmail(dto.getEmail());
            user.setLinkedInUrl(dto.getLinkedInUrl());
            user.setName(dto.getName());
            if (null != dto.getPhoneNumbers()) {
                for (PhoneNumberDTO phDto : dto.getPhoneNumbers()) {
                    if (("Landline".equals(phDto.getPhoneType())) && phDto.getPhoneNumber().contains("-")) {
                        String phoneNumber = phDto.getPhoneNumber();
                        String std = phoneNumber.substring(0, phoneNumber.indexOf("-"));
                        String landline = phoneNumber.substring((phoneNumber.indexOf("-") + 1), phoneNumber.length());
                        user.setStdCode(std);
                        user.setLandline(landline);
                        continue;
                    } else if ("Mobile".equals(phDto.getPhoneType())) {
                        String phoneNumber = phDto.getPhoneNumber();
                        user.setMobile(phoneNumber);
                    }
                }
            }
            RoleDTO role = dto.getRole();
            if (null != role) {
                user.setRole(role.getRoleName());
            }
            user.setUserId(dto.getId());
            users.add(user);
        }
        logger.info("Exiting AccountUserServiceImpl.getAccountUsersForBusinessAccount with response " + users);
        return users;
    }

    public AccountUserResponse createAccountUser(String accountId, AccountUserRequest accountUserRequest, User user) throws UserException {
        logger.info("Entered in AccountUserServiceImpl.createAccountUser with request " + accountUserRequest);

        if (accountUserRequest == null) {
            logger.error("Invalid Request in AccountUserServiceImpl.createAccountUser");
            throw new InvalidRequestException("Invalid Request in AccountUserServiceImpl.createAccountUser");
        }

        UserRequest userRequest = transformToApiRequest(accountUserRequest);
        if (null != user) {
            userRequest.setCreatedBy(user.getEmail());
            userRequest.setUpdatedBy(user.getEmail());
        } else {
            userRequest.setCreatedBy("SYSTEM");
            userRequest.setUpdatedBy("SYSTEM");
        }

        String uri = MessageFormat.format(accountUserCreateEndpoint, accountId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>((new Gson()).toJson(userRequest), headers);
        ResponseEntity<String> response = null;
        AccountUserResponse accountUserResponse = new AccountUserResponse();
        try {
            response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
        } catch (Exception e) {
            logger.error("Exception while invoking url : " + accountUserCreateEndpoint + ", Exception details : " + e);
            throw new UserException("Error in creating Account user");
        }

        logger.info("Response code received : " + response.getStatusCode());

        if (HttpStatus.OK == response.getStatusCode()) {
            String responseMsg = response.getBody();
            AccountUserResponse json = new Gson().fromJson(responseMsg, AccountUserResponse.class);
            // accountUserResponse.setMsg(null != json.getMsg() ? json.getMsg() : "Account user created successfully");
            logger.info("Account user created successfully for request " + accountUserRequest);
            return json;
        }

        else {
            String responseMsg = response.getBody();
            ErrorResponseVo json = new Gson().fromJson(responseMsg, ErrorResponseVo.class);
            logger.error(json.getMessage(), accountUserResponse);
            throw new UserException(null != json.getMessage() ? json.getMessage() : "Account user creation failed");

        }

    }

    private UserRequest transformToApiRequest(AccountUserRequest accountUserRequest) {
        UserRequest request = new UserRequest();
        request.setDesignation(accountUserRequest.getDesignation());
        request.setEmail(accountUserRequest.getEmail());
        request.setLinkedInUrl(accountUserRequest.getLinkedInUrl());
        request.setName(accountUserRequest.getName());
        request.setRole(accountUserRequest.getRole());
        if (null != accountUserRequest.getAccount()) {
            request.setAccountId(accountUserRequest.getAccount().getId());
        }

        // ARIA-99
        List<PhoneNumber> numbers = getPhoneNumbers(accountUserRequest);
        request.setPhoneNumbers(numbers);

        return request;
    }

    private List<PhoneNumber> getPhoneNumbers(AccountUserRequest accountUserRequest) {
        List<PhoneNumber> numbers = new ArrayList<>();
        if (null != accountUserRequest.getMobile() && !accountUserRequest.getMobile().isEmpty()) {
            PhoneNumber number = new PhoneNumber("Mobile", accountUserRequest.getMobile());
            numbers.add(number);
        }

        if (null != accountUserRequest.getStdCode() && !accountUserRequest.getStdCode().isEmpty()
                && null != accountUserRequest.getLandline() && !accountUserRequest.getLandline().isEmpty()) {
            PhoneNumber number = new PhoneNumber("Landline", accountUserRequest.getStdCode() + "-" + accountUserRequest.getLandline());
            numbers.add(number);
        }
        return numbers;
    }

    public AccountUserResponse updateAccountUser(String accountId, AccountUserRequest accountUserRequest, String userId, User user) throws UserException {
        logger.info("Entered in AccountUserServiceImpl.updateAccountUser");

        if (accountUserRequest == null) {
            logger.error("Invalid Request in AccountUserServiceImpl.updateAccountUser");
            throw new InvalidRequestException("Invalid Request in AccountUserServiceImpl.updateAccountUser");
        }

        UserRequest userRequest = transformToApiRequest(accountUserRequest);

        if (null != user) {
            userRequest.setUpdatedBy(user.getEmail());
        } else {
            userRequest.setUpdatedBy("SYSTEM");
        }
        String uri = MessageFormat.format(accountUserUpdateEndpoint, accountId);
        uri = uri + "/" + userId;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>((new Gson()).toJson(userRequest), headers);
        ResponseEntity<String> response = null;
        AccountUserResponse accountUserResponse = new AccountUserResponse();
        try {
            response = restTemplate.exchange(uri, HttpMethod.PUT, request, String.class);
        } catch (Exception e) {
            logger.error("Exception while invoking url : " + accountUserUpdateEndpoint + ", Exception details : " + e);
            throw new UserException("Error in updating Account user");
        }

        if (HttpStatus.OK == response.getStatusCode()) {
            String responseMsg = response.getBody();
            AccountUserResponse json = new Gson().fromJson(responseMsg, AccountUserResponse.class);
            accountUserResponse.setMsg(null != json.getMsg() ? json.getMsg() : "Account user updated successfully");
            logger.info("Account user updated successfully ", accountUserRequest);
            return accountUserResponse;
        }

        else {
            String responseMsg = response.getBody();
            ErrorResponseVo json = new Gson().fromJson(responseMsg, ErrorResponseVo.class);
            logger.error(json.getMessage(), accountUserResponse);
            throw new UserException(null != json.getMessage() ? json.getMessage() : "Account user updation failed");

        }

    }

    @Override
    public AccountUserVO getAccountUserByUserId(String businessAccId, String userId) throws UserException {
        logger.info("Entered AccountUserServiceImpl.getAccountUserForBusinessAccount for account Id " + businessAccId + " and user Id" + userId);
        String uri = MessageFormat.format(accountUserDetailsEndpoint, businessAccId);
        uri = uri + "/" + userId;
        AccountUserDTO dto = null;
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
        } catch (Exception e) {
            logger.error("Exception while invoking url : " + accountUserDetailsEndpoint + ", Exception details : " + e);
            throw new UserException("Error in fetching Account user");
        }
        if (HttpStatus.OK == response.getStatusCode()) {
            String responseMsg = response.getBody();
            dto = new Gson().fromJson(responseMsg, AccountUserDTO.class);
        }

        else {
            String responseMsg = response.getBody();
            ErrorResponseVo json = new Gson().fromJson(responseMsg, ErrorResponseVo.class);
            logger.error("Error in fetching Account user, error : " + json.getMessage());
            throw new UserException(json.getMessage());

        }
        AccountUserVO user = new AccountUserVO();
        AccountNameVO vo = new AccountNameVO();
        vo.setId(Long.parseLong(dto.getAccountId()));
        vo.setName(dto.getAccountName());
        user.setAccount(vo);
        // user.setAccountId(dto.getAccountId());
        user.setDesignation(dto.getDesignation());
        user.setEmail(dto.getEmail());
        user.setLinkedInUrl(dto.getLinkedInUrl());
        user.setName(dto.getName());
        if (null != dto.getPhoneNumbers()) {
            for (PhoneNumberDTO phDto : dto.getPhoneNumbers()) {
                if (("Landline".equals(phDto.getPhoneType())) && phDto.getPhoneNumber().contains("-")) {
                    String phoneNumber = phDto.getPhoneNumber();
                    String std = phoneNumber.substring(0, phoneNumber.indexOf("-"));
                    String landline = phoneNumber.substring(phoneNumber.indexOf("-") + 1, phoneNumber.length());
                    user.setStdCode(std);
                    user.setLandline(landline);
                    continue;
                } else if ("Mobile".equals(phDto.getPhoneType())) {
                    String phoneNumber = phDto.getPhoneNumber();
                    user.setMobile(phoneNumber);
                }
            }
        }
        if (null != dto.getRole()) {
            user.setRole(dto.getRole().getRoleName());
        }
        user.setUserId(dto.getId());
        logger.info("Exiting AccountUserServiceImpl.getAccountUsersForBusinessAccount with response " +
                user);
        return user;
    }

    @Override
    public AccountUserResponse deleteAccountUser(String accountId, String userId) throws UserException {
        logger.info("Entered AccountUserServiceImpl.deleteAccountUser for account Id " + accountId + " and user Id " + userId);
        String uri = MessageFormat.format(accountUserDeleteEndpoint, accountId);
        uri = uri + "/" + userId;
        AccountUserResponse dto = null;
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        } catch (Exception e) {
            logger.error("Exception while invoking url : " + accountUserDeleteEndpoint + ", Exception details : " + e);
            throw new UserException("Error in deleting Account user");
        }
        if (HttpStatus.OK == response.getStatusCode()) {
            String responseMsg = response.getBody();
            dto = new Gson().fromJson(responseMsg, AccountUserResponse.class);
        }

        else {
            String responseMsg = response.getBody();
            ErrorResponseVo json = new Gson().fromJson(responseMsg, ErrorResponseVo.class);
            logger.error("Error in deleting Account user, error : " + json.getMessage());
            throw new UserException(json.getMessage());

        }
        logger.info("Exiting AccountUserServiceImpl.deleteAccountUser for account Id " + accountId + " and user Id " + userId + " with response " + dto);
        return dto;
    }

    @Override
    public AccountUserResponse resetPasswordForAccountUser(String accountId, String userId, ResetPasswordRequest reset, User user) throws UserException {
        logger.info("Entered AccountUserServiceImpl.resetPasswordForAccountUser for account Id " + accountId + " and user Id " + userId);
        String uri = resetPwdEndpoint;
        uri = MessageFormat.format(uri, accountId, userId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (null == reset) {
            reset = new ResetPasswordRequest();
            reset.setCcEmailId(null);
        }
        if (null == user) {
            reset.setRequestedBy("SYSTEM");
        } else {
            reset.setRequestedBy(user.getEmail());
        }
        HttpEntity<ResetPasswordRequest> request = new HttpEntity<ResetPasswordRequest>(reset, headers);
        ResponseEntity<BaseResponse> exchange = restTemplate.exchange(uri, HttpMethod.POST, request,
                BaseResponse.class);
        BaseResponse resp = exchange.getBody();
        if (exchange.getStatusCode().is2xxSuccessful()) {
            AccountUserResponse response = new AccountUserResponse();
            response.setMsg(resp.getMsg());
            logger.info("Entered AccountUserServiceImpl.resetPasswordForAccountUser for account Id " + accountId + " and user Id " + userId);
            return response;
        } else {
            logger.error("Exception in AccountUserServiceImpl.resetPasswordForAccountUser,exception : " + resp.getMsg());
            throw new UserException(resp.getMsg());
        }

    }

    @Override
    public UserAccIdDetail getAccountUserByEmailId(String emailId) throws UserException {
        logger.info("Entered AccountUserServiceImpl.getAccountUserByEmailId for user email Id" + emailId);
        String uri = userDetailsByEmail;
        ResponseEntity<UserAccIdDetail> response = null;
        HttpEntity<String> entity = new HttpEntity<String>(emailId);
        try {
            response = restTemplate.exchange(uri, HttpMethod.POST, entity, UserAccIdDetail.class);
        } catch (Exception e) {
            logger.error("Exception while invoking url : " + uri + ", Exception details : " + e);
            throw new UserException("Error in fetching Account user");
        }
        UserAccIdDetail detail = response.getBody();
        if (null == detail) {
            throw new UserException("No user found with email Id " + emailId);
        }
        logger.info("Exiting AccountUserServiceImpl.getAccountUserByEmailId with response " +
                detail);
        return detail;
    }

}
