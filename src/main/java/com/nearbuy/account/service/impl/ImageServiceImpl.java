package com.nearbuy.account.service.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.nearbuy.account.exception.ImageNotFoundException;
import com.nearbuy.account.exception.UploadException;
import com.nearbuy.account.model.FileMessageResource;
import com.nearbuy.account.model.ImageDTO;
import com.nearbuy.account.model.ImageID;
import com.nearbuy.account.model.ImageResponse;
import com.nearbuy.account.model.MerchantServiceErrorResponse;
import com.nearbuy.account.model.MerchantServiceResponse;
import com.nearbuy.account.service.ImageService;

@Service
// TODO add logging
public class ImageServiceImpl implements ImageService {

    @Value("${imageService.detail.endpoint}")
    private String imageDetailEndpoint;

    @Value("${imageService.fetchImage.endpoint}")
    private String imageFetchEndpoint;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Override
    public ImageDTO fetchImageDetailsFromId(String id) {
        String uri = imageDetailEndpoint + "/" + id;
        ResponseEntity<MerchantServiceResponse<ImageDTO>> entity = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<ImageDTO>>() {
                });
        MerchantServiceResponse<ImageDTO> response = entity.getBody();
        if (null == response || null == response.getRs()) {
            // Image not found - log
            return null;
        }
        ImageDTO dto = response.getRs();
        return dto;
    }

    @Override
    public ImageID uploadImage(MultipartFile file) throws UploadException {
        try {
            final MultiValueMap<String, Object> data = new LinkedMultiValueMap<String, Object>();
            data.add("isSecure", "true");
            FileMessageResource value = new FileMessageResource(file.getBytes(), file.getOriginalFilename());
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG.toString());
            HttpEntity<FileMessageResource> entity = new HttpEntity<FileMessageResource>(value, headers);
            data.add("file", entity);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA.toString());
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(data, httpHeaders);
            ResponseEntity<MerchantServiceResponse<ImageID>> response = restTemplate.exchange(imageDetailEndpoint, HttpMethod.POST, requestEntity,
                    new ParameterizedTypeReference<MerchantServiceResponse<ImageID>>() {
                    });
            if (response.getStatusCode().is2xxSuccessful()) {
                return response.getBody().getRs();
            } else {
                MerchantServiceErrorResponse er = response.getBody().getEr();
                throw new UploadException(er != null ? er.getMessage() : "Exception while uploading image");
            }
        } catch (IOException e) {
            throw new UploadException(e);
        }

    }

    @Override
    public ImageResponse fetchSecuredImage(String id, boolean onlyView) throws ImageNotFoundException {
        String uri = imageFetchEndpoint + String.valueOf(onlyView);
        uri = MessageFormat.format(uri, id);
        ResponseEntity<byte[]> imageBytes = restTemplate.exchange(uri, HttpMethod.GET, null, byte[].class);
        if (null == imageBytes.getBody()) {
            // log this
            return null;
        }
        HttpHeaders headers = imageBytes.getHeaders();
        MediaType contentType = headers.getContentType();
        List<String> list = headers.get("Content-Disposition");
        ImageResponse response = new ImageResponse();
        response.setContentType(contentType);
        response.setContentDisposition(list == null || !list.isEmpty() ? list.get(0) : onlyView == true
                ? "inline" : "attachment");
        response.setImage(imageBytes.getBody());
        return response;

    }
}
