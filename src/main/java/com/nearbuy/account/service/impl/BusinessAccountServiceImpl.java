package com.nearbuy.account.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.nearbuy.account.exception.BusinessAccountException;
import com.nearbuy.account.model.AccountLookupResponse;
import com.nearbuy.account.model.BankAccountApiRequest;
import com.nearbuy.account.model.BankAccountDetailDTO;
import com.nearbuy.account.model.BankResponse;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.BusinessAccountDTO;
import com.nearbuy.account.model.ImageDTO;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantServiceResponse;
import com.nearbuy.account.model.SearchRequest;
import com.nearbuy.account.request.vo.BankAccountRequest;
import com.nearbuy.account.response.vo.AccountApiErrorResponse;
import com.nearbuy.account.response.vo.AccountApiSuccessResponse;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.BusinessAccountVO;
import com.nearbuy.account.service.BusinessAccountService;
import com.nearbuy.account.service.ImageService;
import com.nearbuy.account.util.MapperUtil;

@Service
public class BusinessAccountServiceImpl implements BusinessAccountService {

    private Logger logger = LoggerFactory.getLogger(BusinessAccountServiceImpl.class);

    @Value("${accountService.businessAccountDetails}")
    private String businessAccountDetailsEndpoint;

    @Value("${accountService.businessAccountUpdate}")
    private String businessAccountUpdateEndpoint;

    @Value("${accountService.accountSearch}")
    private String businessAccountSearchEndpoint;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Autowired
    private ImageService imageService;

    // @Autowired
    // private ImageService imageService;

    @Value("${accountService.bankAccountCreate}")
    private String bankDetailEndpoint;

    @Override
    public BusinessAccountVO getDetailsForBusinessAccount(String businessAccId) throws BusinessAccountException {
        logger.info("Entered BusinessAccountServiceImpl.getDetailsForBusinessAccount for request " + businessAccId);
        String uri = businessAccountDetailsEndpoint + businessAccId;
        ResponseEntity<MerchantServiceResponse<BusinessAccountDTO>> exchange = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<BusinessAccountDTO>>() {
                });
        MerchantServiceResponse<BusinessAccountDTO> dto = exchange.getBody();
        BusinessAccountDTO account = null;
        if (HttpStatus.OK != exchange.getStatusCode()) {
            logger.error("Error while fetching Business Account details ");
            throw new BusinessAccountException("Error while fetching Business Account details ");
        } else {
            account = (BusinessAccountDTO) dto.getRs();
        }

        if (null != account && null != account.getBankAccount()) {
            for (BankAccountDetailDTO bank : account.getBankAccount()) {
                setImageNameFromId(bank);
            }
        }

        BusinessAccountVO accountVO = MapperUtil.convertToBusinessAccountViewObject(account);
        if (null != account.getAccountUsers()) {
            accountVO.setAccountUsers(MapperUtil.convertAccountUserResponseToVO(account.getAccountUsers()));
        }

        logger.info("Exiting BusinessAccountServiceImpl.getDetailsForBusinessAccount with response " + account);
        return accountVO;
    }

    private void setImageNameFromId(BankAccountDetailDTO bank) {
        if (null == bank) {
            return;
        }
        ImageDTO dto = imageService.fetchImageDetailsFromId(bank.getCancelledChequeId());
        if (null != dto) {
            bank.setChequeName(dto.getName());
        }
    }

    @Override
    public BaseResponse updateBusinessAccount(String accountId, BusinessAccountDTO accountDTO) throws BusinessAccountException {
        logger.info(
                "Entered BusinessAccountServiceImpl.updateBusinessAccount for account " + accountId + " with request : " + accountDTO.getLegalName());
        String uri = businessAccountUpdateEndpoint + accountId;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BusinessAccountDTO> request = new HttpEntity<BusinessAccountDTO>(accountDTO, headers);
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                String.class);
        if (HttpStatus.OK != exchange.getStatusCode()) {
            AccountApiErrorResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiErrorResponse.class);
            logger.error("Error while updating Business Account details : " + dto.getMessage());
            throw new BusinessAccountException(dto.getMessage());
        } else {
            AccountApiSuccessResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiSuccessResponse.class);
            logger.info("Account services api response : " + dto);
            return new BaseResponse("Business Account with id " + accountId + " updated successfully");
        }
    }

    @Override
    public BaseResponse createBankDetailsForAccount(String accountId, BankAccountRequest detail) throws BusinessAccountException {
        BankAccountApiRequest request = getBankAccountApiRequest(detail);

        String uri = bankDetailEndpoint;
        uri = MessageFormat.format(uri, accountId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BankAccountApiRequest> createRequest = new HttpEntity<BankAccountApiRequest>(request, headers);
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.POST, createRequest,
                String.class);
        if (HttpStatus.OK != exchange.getStatusCode()) {
            AccountApiErrorResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiErrorResponse.class);
            logger.error("Error while creating bank account : " + dto.getMessage());
            throw new BusinessAccountException(dto.getMessage());
        } else {
            AccountApiSuccessResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiSuccessResponse.class);
            logger.info(dto.getMsg());
            return new BankResponse("BankAccount created successfully", dto.getId());
        }

    }

    private BankAccountApiRequest getBankAccountApiRequest(BankAccountRequest detail) {
        BankAccountApiRequest request = new BankAccountApiRequest();
        request.setAccountHolderName(detail.getAcHolderName());
        request.setAccountNickname(detail.getNickname());
        request.setAccountNo(detail.getAcNumber());
        request.setBankName(detail.getName());
        request.setCancelledChequeId(detail.getChequeId());
        request.setIfscCode(detail.getIfsc());
        return request;
    }

    @Override
    public BaseResponse deleteBankDetailsForAccount(String accountId, String bankAccountId) throws BusinessAccountException {
        String uri = bankDetailEndpoint;
        uri = MessageFormat.format(uri, accountId);
        uri = uri + "/" + bankAccountId;
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.DELETE, null,
                String.class);
        if (HttpStatus.OK != exchange.getStatusCode()) {
            AccountApiErrorResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiErrorResponse.class);
            logger.error("Error while deleting bank account : " + dto.getMessage());
            throw new BusinessAccountException(dto.getMessage());
        } else {
            AccountApiSuccessResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiSuccessResponse.class);
            logger.info("BankAccount deleted successfully, api response : " + dto);
            return new BaseResponse("BankAccount deleted successfully");
        }

    }

    @Override
    public List<AccountLookupResponse> lookupAccount(String search) {
        // form Search object
        SearchRequest request = getAccountLookupRequest(search);

        // invoke url and get response
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SearchRequest> searchReq = new HttpEntity<SearchRequest>(request, headers);
        ResponseEntity<MerchantServiceResponse<BusinessAccountDTO[]>> entity = restTemplate.exchange(businessAccountSearchEndpoint, HttpMethod.POST, searchReq,
                new ParameterizedTypeReference<MerchantServiceResponse<BusinessAccountDTO[]>>() {
                });
        MerchantServiceResponse<BusinessAccountDTO[]> body = entity.getBody();
        BusinessAccountDTO[] rs = body.getRs();

        List<AccountLookupResponse> list = new ArrayList<>();
        for (BusinessAccountDTO dto : rs) {
            AccountLookupResponse response = new AccountLookupResponse();
            response.setAcId(dto.getBaId());
            response.setAcName(dto.getName());
            if (null != dto.getMerchant() && !dto.getMerchant().isEmpty()) {
                List<AssociatedMerchantDetail> merchants = new ArrayList<>();
                for (MerchantDTO merc : dto.getMerchant()) {
                    AssociatedMerchantDetail detail = MapperUtil.convertToAssociatedMerchantDetailViewObject(merc);
                    merchants.add(detail);
                }
                response.setMerchants(merchants);
            }
            list.add(response);
        }
        return list;
    }

    private SearchRequest getAccountLookupRequest(String search) {
        SearchRequest request = new SearchRequest();
        request.setProjectionFields(Arrays.asList("baId", "name"));
        request.setSearchOn(Arrays.asList("baId", "name"));
        request.setSize(100);
        request.setTerm(search);
        return request;
    }

    @Override
    public BaseResponse updateBankDetailsForAccount(String accountId, Long bankAcctId, BankAccountRequest request) throws BusinessAccountException {
        BankAccountApiRequest apiRequest = getBankAccountApiRequest(request);
        String uri = bankDetailEndpoint;
        uri = MessageFormat.format(uri, accountId);
        uri = uri + "/" + bankAcctId;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BankAccountApiRequest> updateRequest = new HttpEntity<BankAccountApiRequest>(apiRequest, headers);
        ResponseEntity<String> exchange = restTemplate.exchange(uri, HttpMethod.PUT, updateRequest,
                String.class);
        if (HttpStatus.OK != exchange.getStatusCode()) {
            AccountApiErrorResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiErrorResponse.class);
            logger.error("Error while updating bank account : " + dto.getMessage());
            throw new BusinessAccountException(dto.getMessage());
        } else {
            AccountApiSuccessResponse dto = new Gson().fromJson(exchange.getBody(), AccountApiSuccessResponse.class);
            logger.info(dto.getMsg());
            return new BankResponse("BankAccount updated successfully", dto.getId());
        }
    }

}
