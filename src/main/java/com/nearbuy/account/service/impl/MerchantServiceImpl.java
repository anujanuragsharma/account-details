package com.nearbuy.account.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.nearbuy.account.exception.MerchantException;
import com.nearbuy.account.model.BaseResponse;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantServiceResponse;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.MerchantDetailsVO;
import com.nearbuy.account.service.MerchantService;
import com.nearbuy.account.util.MapperUtil;

@Service
public class MerchantServiceImpl implements MerchantService {

    private Logger logger = LoggerFactory.getLogger(MerchantServiceImpl.class);

    @Value("${merchantService.merchantDetails}")
    private String merchantDetailsEndpoint;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Value("${merchantService.merchantUpdate}")
    private String merchantUpdateEndpoint;

    @Value("${merchantService.merchantPhoneUpdate}")
    private String merchantPhoneUpdateEndpoint;

    @Value("${merchantService.merchantReservationUpdate}")
    private String merchantReservationUpdateEndpoint;

    @Value("${merchantService.merchantsForAccount}")
    private String merchantsForAccEndpoint;

    @Override
    public MerchantDetailsVO getDetailsForMerchant(String merchantId) {
        logger.info("Entered MerchantServiceImpl.getDetailsForMerchant for request " + merchantId);
        String uri = merchantDetailsEndpoint + merchantId;
        ResponseEntity<MerchantServiceResponse<MerchantDTO>> exchange = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO>>() {
                });
        MerchantServiceResponse<MerchantDTO> dto = exchange.getBody();
        MerchantDTO merchant = null;
        if (null != dto.getEr()) {
            logger.error("Error while fetching Merchant details : " + dto.getEr());
        } else {
            merchant = (MerchantDTO) dto.getRs();
        }
        MerchantDetailsVO merchantDetailsVO = MapperUtil.convertToMerchantDetailViewObject(merchant);
        logger.info("Exiting MerchantServiceImpl.getDetailsForMerchant with response " + merchantId);
        return merchantDetailsVO;
    }

    @Override
    public BaseResponse updateMerchant(String merchantId, MerchantDTO chain) throws MerchantException {
        logger.info(
                "Entered MerchantServiceImpl.updateMerchant for account " + merchantId + " with request : " + chain);
        String uri = merchantUpdateEndpoint + merchantId;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MerchantDTO> request = new HttpEntity<MerchantDTO>(chain, headers);
        ResponseEntity<MerchantServiceResponse<MerchantDTO>> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO>>() {
                });
        MerchantServiceResponse<MerchantDTO> dto = exchange.getBody();
        if (null != dto.getEr()) {
            logger.error("Error while updating Merchant details : " + dto.getEr());
            throw new MerchantException(
                    (null != dto.getEr() ? dto.getEr().getMessage() : ("Error while updating Merchant with id " + merchantId)));
        } else {
            logger.info("Merchant with id " + merchantId + " updated successfully");
            return new BaseResponse("Merchant with id " + merchantId + " updated successfully");
        }
    }

    @Override
    public BaseResponse updateMerchantPhoneNumber(String merchantId, String phoneId, PhoneDetailDTO detailDTO) throws MerchantException {
        logger.info(
                "Entered MerchantServiceImpl.updateMerchantPhoneNumber for merchant " + merchantId + " with request : " + detailDTO);
        String uri = MessageFormat.format(merchantPhoneUpdateEndpoint, merchantId, phoneId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PhoneDetailDTO> request = new HttpEntity<PhoneDetailDTO>(detailDTO, headers);
        ResponseEntity<MerchantServiceResponse<PhoneDetailDTO>> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                new ParameterizedTypeReference<MerchantServiceResponse<PhoneDetailDTO>>() {
                });
        MerchantServiceResponse<PhoneDetailDTO> dto = exchange.getBody();
        if (null != dto.getEr()) {
            logger.error("Error while updating phone number with id " + phoneId + " for Merchant with id " + merchantId + ", Error: " + dto.getEr());
            throw new MerchantException(
                    (null != dto.getEr() ? dto.getEr().getMessage()
                            : ("Error while updating phone number with id " + phoneId + " for Merchant with id " + merchantId)));
        } else {
            logger.info("Phone number with id " + phoneId + " for merchant " + merchantId + " updated successfully");
            return new BaseResponse("Phone number with id " + phoneId + " for merchant " + merchantId + " updated successfully");
        }
    }

    @Override
    public BaseResponse updateMerchantReservationNumber(String merchantId, String phoneId, PhoneDetailDTO detailDTO) throws MerchantException {
        logger.info(
                "Entered MerchantServiceImpl.updateMerchantReservationNumber for merchant " + merchantId + " with request : " + detailDTO);
        String uri = MessageFormat.format(merchantReservationUpdateEndpoint, merchantId, phoneId);
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PhoneDetailDTO> request = new HttpEntity<PhoneDetailDTO>(detailDTO, headers);
        ResponseEntity<MerchantServiceResponse<PhoneDetailDTO>> exchange = restTemplate.exchange(uri, HttpMethod.PUT, request,
                new ParameterizedTypeReference<MerchantServiceResponse<PhoneDetailDTO>>() {
                });
        MerchantServiceResponse<PhoneDetailDTO> dto = exchange.getBody();
        if (null != dto.getEr()) {
            logger.error("Error while updating reservation number with id " + phoneId + " for Merchant with id " + merchantId + ", Error: " + dto.getEr());
            throw new MerchantException(
                    (null != dto.getEr() ? dto.getEr().getMessage()
                            : ("Error while updating reservation number with id " + phoneId + " for Merchant with id " + merchantId)));
        } else {
            logger.info("Reservation number with id " + phoneId + " for merchant " + merchantId + " updated successfully");
            return new BaseResponse("Reservation number with id " + phoneId + " for merchant " + merchantId + " updated successfully");
        }
    }

    @Override
    public List<AssociatedMerchantDetail> getMerchantsForAccountId(Long accountId) {
        logger.info("Entered MerchantServiceImpl.getMerchantsForAccountId for request : " + accountId);
        String uri = merchantsForAccEndpoint + accountId;
        ResponseEntity<MerchantServiceResponse<MerchantDTO[]>> exchange = restTemplate.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<MerchantServiceResponse<MerchantDTO[]>>() {
                });
        MerchantServiceResponse<MerchantDTO[]> dto = exchange.getBody();
        MerchantDTO[] merchants = null;
        if (null != dto.getEr()) {
            logger.error("Error while fetching Merchants for Account : " + dto.getEr());
        } else {
            merchants = dto.getRs();
        }
        List<AssociatedMerchantDetail> associatedMerchants = null;
        if (null != merchants) {
            associatedMerchants = new ArrayList<>();
            for (MerchantDTO merchant : merchants) {
                AssociatedMerchantDetail detail = MapperUtil.convertToAssociatedMerchantDetailViewObject(merchant);
                associatedMerchants.add(detail);
            }
        }
        logger.info("Exiting MerchantServiceImpl.getMerchantsForAccountId with response " + associatedMerchants);
        return associatedMerchants;
    }

}
