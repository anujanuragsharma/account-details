package com.nearbuy.account.model;

public class BankResponse extends BaseResponse {

    public BankResponse(String msg, Long id) {
        super(msg);
        this.id = id;
    }

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BankResponse [id=" + id + "]";
    }

}
