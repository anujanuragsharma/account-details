package com.nearbuy.account.model;

import java.util.List;

public class BusinessAccountDTO {

    private Long baId;

    private String accountOwner;

    private String accountCreatedBy;

    private String name;

    private String legalName;

    private String phoneNumber;

    private String emailAddress;

    private AddressDTO businessAddress;

    private List<MerchantDTO> merchant;

    private List<BankAccountDetailDTO> bankAccount;

    private List<AccountUserDTO> accountUsers;

    private String tinNumber;

    private String panNumber;

    private String serviceTaxNumber;

    private String entertainmentTaxNumber;

    private String taxResidencyCertificate;

    private String form10F;

    private String noPEdeclaration;

    public String getServiceTaxNumber() {
        return serviceTaxNumber;
    }

    public void setServiceTaxNumber(String serviceTaxNumber) {
        this.serviceTaxNumber = serviceTaxNumber;
    }

    public String getEntertainmentTaxNumber() {
        return entertainmentTaxNumber;
    }

    public void setEntertainmentTaxNumber(String entertainmentTaxNumber) {
        this.entertainmentTaxNumber = entertainmentTaxNumber;
    }

    public String getTaxResidencyCertificate() {
        return taxResidencyCertificate;
    }

    public void setTaxResidencyCertificate(String taxResidencyCertificate) {
        this.taxResidencyCertificate = taxResidencyCertificate;
    }

    public String getForm10F() {
        return form10F;
    }

    public void setForm10F(String form10f) {
        form10F = form10f;
    }

    public String getNoPEdeclaration() {
        return noPEdeclaration;
    }

    public void setNoPEdeclaration(String noPEdeclaration) {
        this.noPEdeclaration = noPEdeclaration;
    }

    public Long getBaId() {
        return baId;
    }

    public void setBaId(Long baId) {
        this.baId = baId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public AddressDTO getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(AddressDTO businessAddress) {
        this.businessAddress = businessAddress;
    }

    public List<MerchantDTO> getMerchant() {
        return merchant;
    }

    public void setMerchant(List<MerchantDTO> merchant) {
        this.merchant = merchant;
    }

    public List<BankAccountDetailDTO> getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(List<BankAccountDetailDTO> bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getTinNumber() {
        return tinNumber;
    }

    public void setTinNumber(String tinNumber) {
        this.tinNumber = tinNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    public String getAccountCreatedBy() {
        return accountCreatedBy;
    }

    public void setAccountCreatedBy(String accountCreatedBy) {
        this.accountCreatedBy = accountCreatedBy;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BusinessAccountDTO [baId=");
        builder.append(baId);
        builder.append(", accountOwner=");
        builder.append(accountOwner);
        builder.append(", accountCreatedBy=");
        builder.append(accountCreatedBy);
        builder.append(", name=");
        builder.append(name);
        builder.append(", legalName=");
        builder.append(legalName);
        builder.append(", phoneNumber=");
        builder.append(phoneNumber);
        builder.append(", emailAddress=");
        builder.append(emailAddress);
        builder.append(", businessAddress=");
        builder.append(businessAddress);
        builder.append(", merchant=");
        builder.append(merchant);
        builder.append(", bankAccount=");
        builder.append(bankAccount);
        builder.append(", accountUsers=");
        builder.append(accountUsers);
        builder.append(", tinNumber=");
        builder.append(tinNumber);
        builder.append(", panNumber=");
        builder.append(panNumber);
        builder.append(", serviceTaxNumber=");
        builder.append(serviceTaxNumber);
        builder.append(", entertainmentTaxNumber=");
        builder.append(entertainmentTaxNumber);
        builder.append(", taxResidencyCertificate=");
        builder.append(taxResidencyCertificate);
        builder.append(", form10F=");
        builder.append(form10F);
        builder.append(", noPEdeclaration=");
        builder.append(noPEdeclaration);
        builder.append("]");
        return builder.toString();
    }

    public List<AccountUserDTO> getAccountUsers() {
        return accountUsers;
    }

    public void setAccountUsers(List<AccountUserDTO> accountUsers) {
        this.accountUsers = accountUsers;
    }

}
