package com.nearbuy.account.model;

public class PermissionDTO {

    private Long permissionId;

    private String permissionName;

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @Override
    public String toString() {
        return "PermissionDTO [permissionId=" + permissionId + ", permissionName=" + permissionName + "]";
    }

}
