package com.nearbuy.account.model;

public class ResetPasswordRequest {

    private String requestedBy;

    private String ccEmailId;

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getCcEmailId() {
        return ccEmailId;
    }

    public void setCcEmailId(String ccEmailId) {
        this.ccEmailId = ccEmailId;
    }

    @Override
    public String toString() {
        return "ResetPasswordRequest [requestedBy=" + requestedBy + ", ccEmailId=" + ccEmailId + "]";
    }

}
