package com.nearbuy.account.model;

import java.util.List;
import java.util.Map;

public class MerchantDTO {
    private Long merchantId;

    // TODO Account ID is required
    private String businessAccountId;

    private String name;

    private String description;

    private Map<Integer, Timing> timing;

    private List<CategoryInfo> catInfo;

    private String usp;

    private Integer status;

    private String bannerImage;

    private String merchantType;

    private String nickName;

    private List<String> reservationEmailId;

    private List<PhoneDetailDTO> reservationNumbers;

    private List<PhoneDetailDTO> phoneNumbers;

    private List<String> contactEmailId;

    private Boolean isChain;

    private String chainMerchantId;

    private String coverage;

    private String bannerImagePortrait;

    private Long redemptionPolygonId;

    private Long businessPolygonId;

    private AddressDTO redemptionAddress;

    private AddressDTO businessAddress;

    private List<String> sources;

    private List<String> followers;

    private List<String> tags;

    private List<String> rating;

    private String sfId;

    private Boolean isPublished;

    private String dwhId;

    private String websiteLink;

    private Map<String, String> socialMediaLink;

    private String companyLogoId;

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public Map<String, String> getSocialMediaLink() {
        return socialMediaLink;
    }

    public void setSocialMediaLink(Map<String, String> socialMediaLink) {
        this.socialMediaLink = socialMediaLink;
    }

    public String getCompanyLogoId() {
        return companyLogoId;
    }

    public void setCompanyLogoId(String companyLogoId) {
        this.companyLogoId = companyLogoId;
    }

    public MerchantDTO() {
        super();
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getBusinessAccountId() {
        return businessAccountId;
    }

    public void setBusinessAccountId(String businessAccountId) {
        this.businessAccountId = businessAccountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsp() {
        return usp;
    }

    public void setUsp(String usp) {
        this.usp = usp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<String> getReservationEmailId() {
        return reservationEmailId;
    }

    public void setReservationEmailId(List<String> reservationEmailId) {
        this.reservationEmailId = reservationEmailId;
    }

    public List<PhoneDetailDTO> getReservationNumbers() {
        return reservationNumbers;
    }

    public void setReservationNumbers(List<PhoneDetailDTO> reservationNumbers) {
        this.reservationNumbers = reservationNumbers;
    }

    public List<PhoneDetailDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneDetailDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<String> getContactEmailId() {
        return contactEmailId;
    }

    public void setContactEmailId(List<String> contactEmailId) {
        this.contactEmailId = contactEmailId;
    }

    public Boolean getIsChain() {
        return isChain;
    }

    public void setIsChain(Boolean isChain) {
        this.isChain = isChain;
    }

    public String getChainMerchantId() {
        return chainMerchantId;
    }

    public void setChainMerchantId(String chainMerchantId) {
        this.chainMerchantId = chainMerchantId;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getBannerImagePortrait() {
        return bannerImagePortrait;
    }

    public void setBannerImagePortrait(String bannerImagePortrait) {
        this.bannerImagePortrait = bannerImagePortrait;
    }

    public Long getRedemptionPolygonId() {
        return redemptionPolygonId;
    }

    public void setRedemptionPolygonId(Long redemptionPolygonId) {
        this.redemptionPolygonId = redemptionPolygonId;
    }

    public Long getBusinessPolygonId() {
        return businessPolygonId;
    }

    public void setBusinessPolygonId(Long businessPolygonId) {
        this.businessPolygonId = businessPolygonId;
    }

    public AddressDTO getRedemptionAddress() {
        return redemptionAddress;
    }

    public void setRedemptionAddress(AddressDTO redemptionAddress) {
        this.redemptionAddress = redemptionAddress;
    }

    public AddressDTO getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(AddressDTO businessAddress) {
        this.businessAddress = businessAddress;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getRating() {
        return rating;
    }

    public void setRating(List<String> rating) {
        this.rating = rating;
    }

    public String getSfId() {
        return sfId;
    }

    public void setSfId(String sfId) {
        this.sfId = sfId;
    }

    public Boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    public String getDwhId() {
        return dwhId;
    }

    public void setDwhId(String dwhId) {
        this.dwhId = dwhId;
    }

    public Map<Integer, Timing> getTiming() {
        return timing;
    }

    public void setTiming(Map<Integer, Timing> timing) {
        this.timing = timing;
    }

    @Override
    public String toString() {
        return "MerchantDTO [merchantId=" + merchantId + ", businessAccountId=" + businessAccountId + ", name=" + name + ", description=" + description
                + ", timing=" + timing + ", usp=" + usp + ", status=" + status + ", bannerImage=" + bannerImage + ", merchantType=" + merchantType
                + ", nickName=" + nickName + ", reservationEmailId=" + reservationEmailId + ", reservationNumbers=" + reservationNumbers + ", phoneNumbers="
                + phoneNumbers + ", contactEmailId=" + contactEmailId + ", isChain=" + isChain + ", chainMerchantId=" + chainMerchantId + ", coverage="
                + coverage + ", bannerImagePortrait=" + bannerImagePortrait + ", redemptionPolygonId=" + redemptionPolygonId + ", businessPolygonId="
                + businessPolygonId + ", redemptionAddress=" + redemptionAddress + ", businessAddress=" + businessAddress + ", sources=" + sources
                + ", followers=" + followers + ", tags=" + tags + ", rating=" + rating + ", sfId=" + sfId + ", isPublished=" + isPublished + ", dwhId=" + dwhId
                + ", websiteLink=" + websiteLink + ", socialMediaLink=" + socialMediaLink + ", companyLogoId=" + companyLogoId + "]";
    }

    public List<CategoryInfo> getCatInfo() {
        return catInfo;
    }

    public void setCatInfo(List<CategoryInfo> catInfo) {
        this.catInfo = catInfo;
    }

}
