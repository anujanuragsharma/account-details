package com.nearbuy.account.model;

public class BankAccountApiRequest {

    private Long id;

    private String accountNickname;

    private String cancelledChequeId;

    private String branch;

    private String bankName;

    private String accountNo;

    private String ifscCode;

    private String accountHolderName;

    public String getAccountNickname() {
        return accountNickname;
    }

    public void setAccountNickname(String accountNickname) {
        this.accountNickname = accountNickname;
    }

    public String getCancelledChequeId() {
        return cancelledChequeId;
    }

    public void setCancelledChequeId(String cancelledChequeId) {
        this.cancelledChequeId = cancelledChequeId;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    @Override
    public String toString() {
        return "BankAccountApiRequest [_id=" + id + ", accountNickname=" + accountNickname + ", cancelledChequeId=" + cancelledChequeId + ", branch=" + branch
                + ", bankName=" + bankName + ", accountNo=" + accountNo + ", ifscCode=" + ifscCode + ", accountHolderName=" + accountHolderName + "]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
