package com.nearbuy.account.model;

public class SearchFilter {

    private boolean isChain;

    public boolean isChain() {
        return isChain;
    }

    public void setChain(boolean isChain) {
        this.isChain = isChain;
    }

    @Override
    public String toString() {
        return "SearchFilter [isChain=" + isChain + "]";
    }

}
