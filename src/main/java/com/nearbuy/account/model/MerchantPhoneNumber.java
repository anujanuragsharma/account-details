package com.nearbuy.account.model;

public class MerchantPhoneNumber {

    private Long id;

    private String type;

    private String countryCode;

    private String stdCode;

    private String value;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "MerchantPhoneNumber [id=" + id + ", type=" + type + ", countryCode=" + countryCode + ", stdCode=" + stdCode + ", value=" + value + "]";
    }

}
