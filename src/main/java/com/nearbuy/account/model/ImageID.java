package com.nearbuy.account.model;

public class ImageID {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ImageID [id=" + id + "]";
    }

}
