package com.nearbuy.account.model;

public class UserAccIdDetail {

    private String accId;

    private Long id;

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserAccIdDetail [accId=" + accId + ", id=" + id + "]";
    }

}
