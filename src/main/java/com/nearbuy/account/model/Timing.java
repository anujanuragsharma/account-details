package com.nearbuy.account.model;

import java.io.Serializable;
import java.util.List;

/**
 * To manage the start and end time of the merchant for a day.
 * Time here will be calculated in seconds after 23h:59m:59s of previous day
 * 
 * @author Sunny
 *
 */
public class Timing implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2854953720725599834L;
	
	/**
	 * List of time slots
	 */
	private List<TimeSlot> timeSlots;
	
	/**
	 * Whether merchant is open or closed for the "day"
	 */
	private Boolean isOpen;
	
	/**
	 * Whether merchant is open entire day
	 */
	private Boolean isOpenAllDay;

	public Boolean getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}

	/**
	 * @return the isOpenAllDay
	 */
	public Boolean getIsOpenAllDay() {
		return isOpenAllDay;
	}

	/**
	 * @param isOpenAllDay the isOpenAllDay to set
	 */
	public void setIsOpenAllDay(Boolean isOpenAllDay) {
		this.isOpenAllDay = isOpenAllDay;
	}

	/**
	 * @return the timeSlots
	 */
	public List<TimeSlot> getTimeSlots() {
		return timeSlots;
	}

	/**
	 * @param timeSlots the timeSlots to set
	 */
	public void setTimeSlots(List<TimeSlot> timeSlots) {
		this.timeSlots = timeSlots;
	}
	
}
