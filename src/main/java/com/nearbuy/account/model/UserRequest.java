package com.nearbuy.account.model;

import java.util.List;

import com.nearbuy.account.request.vo.PhoneNumber;

public class UserRequest {

    private String name;

    private String email;

    private String role;

    private List<PhoneNumber> phoneNumbers;

    private String linkedInUrl;

    private String designation;

    private String createdBy;

    private String updatedBy;

    private Long accountId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserRequest [name=");
        builder.append(name);
        builder.append(", email=");
        builder.append(email);
        builder.append(", role=");
        builder.append(role);
        builder.append(", phoneNumbers=");
        builder.append(phoneNumbers);
        builder.append(", linkedInUrl=");
        builder.append(linkedInUrl);
        builder.append(", designation=");
        builder.append(designation);
        builder.append(", createdBy=");
        builder.append(createdBy);
        builder.append(", updatedBy=");
        builder.append(updatedBy);
        builder.append(", accountId=");
        builder.append(accountId);
        builder.append("]");
        return builder.toString();
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

}
