package com.nearbuy.account.model;

import java.util.List;
import java.util.Map;

public class SearchRequest {

    private List<String> projectionFields;

    private String term;

    private List<String> searchOn;

    private Map<String, Integer> sortFields;

    private SearchFilter filter;

    private int size;

    public List<String> getProjectionFields() {
        return projectionFields;
    }

    public void setProjectionFields(List<String> projectionFields) {
        this.projectionFields = projectionFields;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public List<String> getSearchOn() {
        return searchOn;
    }

    public void setSearchOn(List<String> searchOn) {
        this.searchOn = searchOn;
    }

    public SearchFilter getFilter() {
        return filter;
    }

    public void setFilter(SearchFilter filter) {
        this.filter = filter;
    }

    public Map<String, Integer> getSortFields() {
        return sortFields;
    }

    public void setSortFields(Map<String, Integer> sortFields) {
        this.sortFields = sortFields;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SearchRequest [projectionFields=");
        builder.append(projectionFields);
        builder.append(", term=");
        builder.append(term);
        builder.append(", searchOn=");
        builder.append(searchOn);
        builder.append(", sortFields=");
        builder.append(sortFields);
        builder.append(", filter=");
        builder.append(filter);
        builder.append(", size=");
        builder.append(size);
        builder.append("]");
        return builder.toString();
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
