package com.nearbuy.account.model;

public class BankAccountDetailDTO {

    private Long id;

    private String accountNo;

    private String ifscCode;

    private String bankName;

    private String branch;

    private String accountNickname;

    private String accountHolderName;

    private boolean isActive;

    private String cancelledChequeId;
    
    private String chequeName;
    
    public BankAccountDetailDTO() {

    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountNickname() {
        return accountNickname;
    }

    public void setAccountNickname(String accountNickname) {
        this.accountNickname = accountNickname;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the _id
     */

    public String getCancelledChequeId() {
        return cancelledChequeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCancelledChequeId(String cancelledChequeId) {
        this.cancelledChequeId = cancelledChequeId;
    }

    public String getChequeName() {
        return chequeName;
    }

    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }

}
