package com.nearbuy.account.model;

import java.util.Collection;
import java.util.Date;

public class AccountUserDTO {

    private Long id;

    private String accountId;

    private String accountName;

    private String linkedInUrl;

    private String designation;

    private String email;

    private Collection<PhoneNumberDTO> phoneNumbers;

    private String password;

    private String updatedBy;

    private boolean isActive;

    // private Collection<String> merchantIds;

    private Long updatedAt;

    private String resetPasswordToken;

    private Date lastLogin;

    private String name;

    private String createdBy;

    private Long createdAt;

    private RoleDTO role;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getLinkedInUrl() {
        return linkedInUrl;
    }

    public void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Collection<PhoneNumberDTO> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Collection<PhoneNumberDTO> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return isActive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*
     * public Collection<String> getMerchantIds() {
     * return merchantIds;
     * }
     * 
     * public void setMerchantIds(Collection<String> merchantIds) {
     * this.merchantIds = merchantIds;
     * }
     */

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AccountUserDTO [id=" + id + ", accountId=" + accountId + ", linkedInUrl=" + linkedInUrl + ", designation=" + designation + ", email=" + email
                + ", phoneNumbers=" + phoneNumbers + ", password=" + password + ", updatedBy=" + updatedBy + ", isActive=" + isActive + ", updatedAt="
                + updatedAt + ", resetPasswordToken=" + resetPasswordToken + ", lastLogin=" + lastLogin + ", name=" + name + ", createdBy=" + createdBy
                + ", createdAt=" + createdAt + ", role=" + role + "]";
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

}
