package com.nearbuy.account.model;

import java.util.List;

import com.nearbuy.account.response.vo.AssociatedMerchantDetail;

public class AccountLookupResponse {
    private String acName;
    private Long acId;
    private List<AssociatedMerchantDetail> merchants;

    public String getAcName() {
        return acName;
    }

    public void setAcName(String acName) {
        this.acName = acName;
    }

    public Long getAcId() {
        return acId;
    }

    public void setAcId(Long acId) {
        this.acId = acId;
    }

    public List<AssociatedMerchantDetail> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<AssociatedMerchantDetail> merchants) {
        this.merchants = merchants;
    }

    @Override
    public String toString() {
        return "AccountLookupResponse [acName=" + acName + ", acId=" + acId + ", merchants=" + merchants + "]";
    }

}
