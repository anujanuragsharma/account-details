package com.nearbuy.account.model;

public class MerchantServiceResponse<T> {

    private Integer st;
    private MerchantServiceErrorResponse er;
    private T rs;

    public Integer getSt() {
        return st;
    }

    public void setSt(Integer st) {
        this.st = st;
    }

    public MerchantServiceErrorResponse getEr() {
        return er;
    }

    public void setEr(MerchantServiceErrorResponse er) {
        this.er = er;
    }

    public T getRs() {
        return rs;
    }

    public void setRs(T rs) {
        this.rs = rs;
    }

    @Override
    public String toString() {
        return "MerchantServiceResponse [st=" + st + ", er=" + er + ", rs=" + rs + "]";
    }

}
