package com.nearbuy.account.model;

public class MerchantServiceErrorResponse {

    private String code;

    private String message;

    private String stacktrace;

    public MerchantServiceErrorResponse() {
        // TODO Auto-generated constructor stub
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public MerchantServiceErrorResponse(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public MerchantServiceErrorResponse(String code, String message, String stacktrace) {
        this(code, message);
        this.stacktrace = stacktrace;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MerchantServiceErrorResponse [code=" + code + ", message=" + message + ", stacktrace=" + stacktrace + "]";
    }

}
