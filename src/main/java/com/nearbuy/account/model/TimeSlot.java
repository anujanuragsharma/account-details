package com.nearbuy.account.model;

import java.io.Serializable;

/**
 * To manage the start and end time of the merchant for a day.
 * Time here will be calculated in seconds after 23h:59m:59s of previous day
 * 
 * @author Sunny
 *
 */
public class TimeSlot implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8287548183313431675L;

	/**
	 * Start time in string as it is e.g. 09:00
	 */
	private String startTimeStr;
	
	/**
	 * End time in string as it is e.g. 09:00
	 */
	private String endTimeStr;
	
	/**
	 * Start Time in seconds lapsed from start of day
	 */
	private Integer startTime;
	
	/**
	 * End Time in seconds lapsed from start of day
	 */
	private Integer endTime;
	
	/**
	 * Start Meridian AM or PM
	 */
	private String startMeridian;
	
	/**
	 * End Meridian AM or PM
	 */
	private String endMeridian;
	
	/**
	 * Default constructor
	 */
	public TimeSlot()
	{
		this.startTime = null;
		this.endTime = null;
		//this.startTimeStr = "11:00";
		//this.endTimeStr = "10:30";
		this.startMeridian = "am";
		this.endMeridian = "pm";
	}

	public String getStartTimeStr() {
		return startTimeStr;
	}

	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public Integer getStartTime() {
		return startTime;
	}

	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the startMeridian
	 */
	public String getStartMeridian() {
		return startMeridian;
	}

	/**
	 * @param startMeridian the startMeridian to set
	 */
	public void setStartMeridian(String startMeridian) {
		this.startMeridian = startMeridian;
	}

	/**
	 * @return the endMeridian
	 */
	public String getEndMeridian() {
		return endMeridian;
	}

	/**
	 * @param endMeridian the endMeridian to set
	 */
	public void setEndMeridian(String endMeridian) {
		this.endMeridian = endMeridian;
	}
	
}
