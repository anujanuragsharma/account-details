package com.nearbuy.account.model;

import java.util.Collection;

public class RoleDTO {
    private String roleName;

    private Long roleId;

    private Collection<PermissionDTO> permissions;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long id) {
        this.roleId = id;
    }

    public Collection<PermissionDTO> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<PermissionDTO> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "RoleDTO [roleName=" + roleName + ", roleId=" + roleId + ", permissions=" + permissions + "]";
    }

}
