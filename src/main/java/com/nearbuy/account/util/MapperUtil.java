package com.nearbuy.account.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.nearbuy.account.model.AccountUserDTO;
import com.nearbuy.account.model.AddressDTO;
import com.nearbuy.account.model.BankAccountDetailDTO;
import com.nearbuy.account.model.BusinessAccountDTO;
import com.nearbuy.account.model.CategoryInfo;
import com.nearbuy.account.model.MerchantDTO;
import com.nearbuy.account.model.MerchantPhoneNumber;
import com.nearbuy.account.model.PhoneDetailDTO;
import com.nearbuy.account.model.PhoneNumberDTO;
import com.nearbuy.account.model.RoleDTO;
import com.nearbuy.account.model.TimeSlot;
import com.nearbuy.account.model.Timing;
import com.nearbuy.account.response.vo.AccountNameVO;
import com.nearbuy.account.response.vo.AccountUserVO;
import com.nearbuy.account.response.vo.AssociatedMerchantDetail;
import com.nearbuy.account.response.vo.BankAccountDetail;
import com.nearbuy.account.response.vo.BusinessAccountVO;
import com.nearbuy.account.response.vo.ChainDetailsVO;
import com.nearbuy.account.response.vo.ContactInfoVO;
import com.nearbuy.account.response.vo.Day;
import com.nearbuy.account.response.vo.MerchantDetailsVO;
import com.nearbuy.account.response.vo.PhoneNumbers;
import com.nearbuy.account.response.vo.SocialNetworkLink;
import com.nearbuy.account.response.vo.SocialNetworkType;
import com.nearbuy.account.response.vo.TimingVO;

public class MapperUtil {

    public static BusinessAccountVO convertToBusinessAccountViewObject(BusinessAccountDTO account) {
        if (null == account) {
            return null;
        }
        BusinessAccountVO vo = new BusinessAccountVO();
        vo.setId(account.getBaId());
        String address = getSingleLineAddress(account.getBusinessAddress());
        vo.setAddress(address);

        // Adding both accountOwner and accountCreatedBy
        Set<String> set = new HashSet<>();
        set.add(account.getAccountCreatedBy());
        set.add(account.getAccountOwner());
        List<String> associatedSalesRep = new ArrayList<>();
        associatedSalesRep.addAll(set);
        vo.setAssociatedSalesRep(associatedSalesRep);
        String phoneNumber = account.getPhoneNumber();
        // if (null != phoneNumber) {
        // MerchantPhoneNumber number = transformToPhoneViewObject(phoneNumber);
        // vo.setPhoneNumber(number);
        // }
        vo.setPhoneNumber(phoneNumber);
        vo.setEmailAddress(account.getEmailAddress());
        vo.setLegalName(account.getLegalName());
        if (null != account.getMerchant()) {
            List<AssociatedMerchantDetail> merchants = new ArrayList<AssociatedMerchantDetail>();
            for (MerchantDTO dto : account.getMerchant()) {
                merchants.add(convertToAssociatedMerchantDetailViewObject(dto));
            }
            vo.setMerchants(merchants);
        }

        vo.setName(account.getName());

        if (null != account.getBankAccount()) {
            List<BankAccountDetail> bankDetails = new ArrayList<BankAccountDetail>();

            for (BankAccountDetailDTO dto : account.getBankAccount()) {
                bankDetails.add(convertToBankDetailViewObject(dto));
            }
            vo.setBankDetails(bankDetails);
        }
        return vo;
    }

    private static MerchantPhoneNumber transformToPhoneViewObject(String phoneNumber) {
        MerchantPhoneNumber number = new MerchantPhoneNumber();
        number.setType("Mobile");
        if (phoneNumber.length() <= 10) {
            number.setCountryCode("+91");
            number.setValue(phoneNumber);
        } else {
            String countryCode = phoneNumber.substring(0, phoneNumber.length() - 10);
            number.setCountryCode(countryCode);
            number.setValue(phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length()));
        }
        return number;
    }

    public static String getSingleLineAddress(AddressDTO address) {
        if (null == address) {
            return null;
        }
        String line1 = (null == address.getAddressLine1() ? "" : address.getAddressLine1());
        String subLocality = (null == address.getSubLocality() ? "" : address.getSubLocality());
        String locality = (null == address.getLocality() ? "" : address.getLocality());
        String city = (null == address.getCityTown() ? "" : address.getCityTown());
        String state = (null == address.getState() ? "" : address.getState());

        String completeAddress = "";
        completeAddress = (line1.equals("") ? "" : line1 + ", ");
        completeAddress = completeAddress + (subLocality.equals("") ? "" : subLocality + ", ");
        completeAddress = completeAddress + (locality.equals("") ? "" : locality + ", ");
        completeAddress = completeAddress + (city.equals("") ? "" : city + ", ");
        completeAddress = completeAddress + (state.equals("") ? "" : state);

        return completeAddress;

    }

    public static AssociatedMerchantDetail convertToAssociatedMerchantDetailViewObject(MerchantDTO dto) {
        if (null == dto) {
            return null;
        }
        AssociatedMerchantDetail detail = new AssociatedMerchantDetail();
        detail.setAddress(getSingleLineAddress(dto.getBusinessAddress()));
        detail.setName(dto.getName());
        List<String> phoneNumbers = new ArrayList<String>();
        for (PhoneDetailDTO ph : dto.getPhoneNumbers()) {
            phoneNumbers.add(ph.getCountryCode() + ph.getStdCode() + ph.getContactNo());
        }
        detail.setPhoneNumbers(phoneNumbers);
        detail.setMerchantId(dto.getMerchantId());
        return detail;
    }

    public static BankAccountDetail convertToBankDetailViewObject(BankAccountDetailDTO dto) {
        if (null == dto) {
            return null;
        }
        BankAccountDetail detail = new BankAccountDetail();
        detail.setBankAcctId(dto.getId());
        detail.setName(dto.getBankName());
        detail.setAcNumber(dto.getAccountNo());
        detail.setAcHolderName(dto.getAccountHolderName());
        detail.setNickname(dto.getAccountNickname());
        detail.setIfsc(dto.getIfscCode());
        // dummy
        detail.setChequeS3Url(dto.getCancelledChequeId());
        detail.setChequeName(dto.getChequeName());
        return detail;
    }

    public static MerchantDetailsVO convertToMerchantDetailViewObject(MerchantDTO dto) {
        if (null == dto) {
            return null;
        }
        MerchantDetailsVO vo = new MerchantDetailsVO();
        vo.setAboutMerchant(dto.getDescription());
        // account id is not being populated currently
        vo.setAccountId(dto.getBusinessAccountId());
        // List<String> associatedSalesRep = null;
        // vo.setAssociatedSalesRep(associatedSalesRep);
        vo.setChainId(dto.getChainMerchantId());
        List<String> followers = dto.getFollowers();
        if (null != followers && !followers.isEmpty()) {
            vo.setFollowers(followers.get(0));
        }
        // else {
        // vo.setFollowers("dummyValue");
        // }
        vo.setId(dto.getMerchantId());
        // bannerImage or bannerPortraitImage
        // need to fetch Images based on the logo Id
        vo.setLogoUrl(dto.getCompanyLogoId());
        vo.setMerchantType(dto.getMerchantType());
        vo.setName(dto.getName());
        vo.setNickName(dto.getNickName());
        setPrimaryAndSecondaryCategory(vo, dto.getCatInfo());
        // vo.setPrimaryCat("dummyPrimaryCategory");
        List<String> rating = dto.getRating();
        if (null != rating && !rating.isEmpty()) {
            vo.setRating(rating.get(0));
        }
        // else {
        // vo.setRating("dummyRating");
        // }
        vo.setRedemptionAddress(getSingleLineAddress(dto.getRedemptionAddress()));
        // vo.setSecondaryCat("dummySecondaryCategory");
        vo.setTags(dto.getTags());
        Map<Integer, Timing> timing = dto.getTiming();
        Set<Integer> set = timing.keySet();
        List<TimingVO> timings = new ArrayList<TimingVO>();
        for (Integer i : set) {
            Timing t = timing.get(i);
            TimingVO timingVO = getTimingVO(i, t);
            timings.add(timingVO);
        }
        vo.setTimings(timings);
        vo.setUsp(dto.getUsp());
        ContactInfoVO infoVO = getContactInfoVO(dto);
        vo.setContactInfo(infoVO);
        return vo;
    }

    private static void setPrimaryAndSecondaryCategory(MerchantDetailsVO vo, List<CategoryInfo> list) {
        if (null == list || list.isEmpty()) {
            return;
        }
        StringBuffer primary = new StringBuffer();
        StringBuffer secondary = new StringBuffer();
        for (CategoryInfo info : list) {
            if (info.getIsPrimary()) {
                String prim = MerchantCategory.getMerchantCategoryDisplay(info.getKey());
                primary.append(prim == null ? "" : prim + ",");
            } else {
                String second = MerchantCategory.getMerchantCategoryDisplay(info.getKey());
                secondary.append(second == null ? "" : second + ",");
            }
        }
        // remove last ',' from both primary and secondary
        String pS = primary.toString();
        pS = pS.substring(0, pS.lastIndexOf(",") != -1 ? pS.lastIndexOf(",") : pS.length());

        String sS = secondary.toString();
        sS = sS.substring(0, sS.lastIndexOf(",") != -1 ? sS.lastIndexOf(",") : sS.length());

        vo.setPrimaryCat(null == pS ? null : pS);
        vo.setSecondaryCat(null == sS ? null : sS);

    }

    private static void setPrimaryAndSecondaryCategory(ChainDetailsVO vo, List<CategoryInfo> list) {
        if (null == list || list.isEmpty()) {
            return;
        }
        StringBuffer primary = new StringBuffer();
        StringBuffer secondary = new StringBuffer();
        for (CategoryInfo info : list) {
            if (info.getIsPrimary()) {
                String prim = MerchantCategory.getMerchantCategoryDisplay(info.getKey());
                primary.append(prim == null ? "" : prim + ",");
            } else {
                String second = MerchantCategory.getMerchantCategoryDisplay(info.getKey());
                secondary.append(second == null ? "" : second + ",");
            }
        }
        // remove last ',' from both primary and secondary
        String pS = primary.toString();
        pS = pS.substring(0, pS.lastIndexOf(",") != -1 ? pS.lastIndexOf(",") : pS.length());

        String sS = secondary.toString();
        sS = sS.substring(0, sS.lastIndexOf(",") != -1 ? sS.lastIndexOf(",") : sS.length());

        vo.setPrimaryCat(null == pS || "".equals(pS) ? null : pS);
        vo.setSecondaryCat(null == sS || "".equals(sS) ? null : sS);

    }

    private static ContactInfoVO getContactInfoVO(MerchantDTO dto) {
        ContactInfoVO vo = new ContactInfoVO();
        vo.setEmailId(dto.getContactEmailId());
        vo.setReservationEmailId(dto.getReservationEmailId());

        if (null != dto.getPhoneNumbers()) {
            PhoneNumbers phoneNumbers = new PhoneNumbers();
            List<MerchantPhoneNumber> mobile = new ArrayList<>();
            List<MerchantPhoneNumber> landline = new ArrayList<>();
            List<MerchantPhoneNumber> tollfree = new ArrayList<>();

            for (PhoneDetailDTO ph : dto.getPhoneNumbers()) {
                MerchantPhoneNumber number = new MerchantPhoneNumber();
                number.setCountryCode(ph.getCountryCode());
                number.setId(ph.getId());
                number.setStdCode(ph.getStdCode());
                number.setValue(ph.getContactNo());
                number.setType(ph.getType());
                if ("Mob".equals(ph.getType())) {
                    mobile.add(number);
                } else if ("Lan".equals(ph.getType())) {
                    landline.add(number);
                } else if ("Tol".equals(ph.getType())) {
                    tollfree.add(number);
                }
            }
            phoneNumbers.setLandline(landline);
            phoneNumbers.setMobile(mobile);
            phoneNumbers.setTollfree(tollfree);
            vo.setPhoneNumbers(phoneNumbers);
        }
        if (null != dto.getReservationNumbers()) {
            PhoneNumbers phoneNumbers = new PhoneNumbers();
            List<MerchantPhoneNumber> mobile = new ArrayList<>();
            List<MerchantPhoneNumber> landline = new ArrayList<>();
            List<MerchantPhoneNumber> tollfree = new ArrayList<>();

            for (PhoneDetailDTO ph : dto.getReservationNumbers()) {
                MerchantPhoneNumber number = new MerchantPhoneNumber();
                number.setCountryCode(ph.getCountryCode());
                number.setId(ph.getId());
                number.setStdCode(ph.getStdCode());
                number.setValue(ph.getContactNo());
                number.setType(ph.getType());
                if ("Mob".equals(ph.getType())) {
                    mobile.add(number);
                } else if ("Lan".equals(ph.getType())) {
                    landline.add(number);
                } else if ("Tol".equals(ph.getType())) {
                    tollfree.add(number);
                }
            }
            phoneNumbers.setLandline(landline);
            phoneNumbers.setMobile(mobile);
            phoneNumbers.setTollfree(tollfree);
            vo.setReservationNumber(phoneNumbers);
        }
        vo.setWebsite(dto.getWebsiteLink());

        Map<String, String> map = dto.getSocialMediaLink();
        if (null != map) {
            Set<String> set = map.keySet();
            List<SocialNetworkLink> links = new ArrayList<>();
            for (String s : set) {
                String url = map.get(s);
                links.add(getSocialNetworkType(s, url));
            }
            vo.setSocialNetworkLinks(links);
        }
        return vo;
    }

    private static SocialNetworkLink getSocialNetworkType(String s, String url) {
        SocialNetworkType type = SocialNetworkType.getSocialNetworkType(s);
        SocialNetworkLink link = new SocialNetworkLink();
        link.setSocialNetworkType(type);
        link.setUrl(url);
        return link;
    }

    private static TimingVO getTimingVO(Integer i, Timing t) {
        TimingVO timingVO = new TimingVO();
        Day day = Day.getDay(i);
        timingVO.setDay(day);
        List<TimeSlot> timeSlots = t.getTimeSlots();
        if (null == timeSlots || timeSlots.isEmpty()) {
            return null;
        }
        TimeSlot timeSlot = timeSlots.get(0);
        timingVO.setCloseTime(timeSlot.getEndTimeStr() + " " + timeSlot.getEndMeridian());
        timingVO.setOpenTime(timeSlot.getStartTimeStr() + " " + timeSlot.getStartMeridian());
        return timingVO;
    }

    public static ChainDetailsVO convertToChainDetailViewObject(MerchantDTO chain) {
        ChainDetailsVO vo = new ChainDetailsVO();
        vo.setAboutMerchant(chain.getDescription());
        vo.setBrandBannerImg(chain.getBannerImage());
        vo.setBusinessAddress(getSingleLineAddress(chain.getBusinessAddress()));
        List<String> imgUrls = new ArrayList<>();

        // TODO to be reviewed again
        imgUrls.add(chain.getBannerImage());
        vo.setChainImgs(imgUrls);

        vo.setChainName(chain.getName());
        vo.setEmailIds(chain.getContactEmailId());
        vo.setId(chain.getMerchantId());
        vo.setLogoUrl(chain.getCompanyLogoId());
        if (null != chain.getPhoneNumbers()) {
            PhoneNumbers phoneNumbers = new PhoneNumbers();
            List<MerchantPhoneNumber> mobile = new ArrayList<>();
            List<MerchantPhoneNumber> landline = new ArrayList<>();
            List<MerchantPhoneNumber> tollfree = new ArrayList<>();

            for (PhoneDetailDTO ph : chain.getPhoneNumbers()) {
                MerchantPhoneNumber number = new MerchantPhoneNumber();
                number.setCountryCode(ph.getCountryCode());
                number.setId(ph.getId());
                number.setStdCode(ph.getStdCode());
                number.setValue(ph.getContactNo());
                number.setType(ph.getType());
                if ("Mob".equals(ph.getType())) {
                    mobile.add(number);
                } else if ("Lan".equals(ph.getType())) {
                    landline.add(number);
                } else if ("Tol".equals(ph.getType())) {
                    tollfree.add(number);
                }
            }
            phoneNumbers.setLandline(landline);
            phoneNumbers.setMobile(mobile);
            phoneNumbers.setTollfree(tollfree);
            vo.setPhoneNumbers(phoneNumbers);
        }
        // vo.setPrimaryCat("dummyPrimary");
        // vo.setSecondaryCat("dummySecondary");
        setPrimaryAndSecondaryCategory(vo, chain.getCatInfo());
        return vo;
    }

    public static List<AccountUserVO> convertAccountUserResponseToVO(List<AccountUserDTO> list) {
        List<AccountUserVO> users = new ArrayList<AccountUserVO>();

        for (AccountUserDTO dto : list) {
            AccountUserVO user = new AccountUserVO();
            AccountNameVO vo = new AccountNameVO();
            vo.setId(Long.parseLong(dto.getAccountId()));
            vo.setName(dto.getAccountName());
            user.setAccount(vo);
            user.setDesignation(dto.getDesignation());
            user.setEmail(dto.getEmail());
            user.setLinkedInUrl(dto.getLinkedInUrl());
            user.setName(dto.getName());
            // List<PhoneNumber> numbers = new ArrayList<PhoneNumber>();
            if (null != dto.getPhoneNumbers()) {
                for (PhoneNumberDTO phDto : dto.getPhoneNumbers()) {
                    // PhoneNumber number = new PhoneNumber();
                    // number.setPhoneNumber(phDto.getPhoneNumber());
                    // number.setPhoneType(phDto.getPhoneType());
                    // numbers.add(number);
                    if (("Landline".equals(phDto.getPhoneType())) && phDto.getPhoneNumber().contains("-")) {
                        String phoneNumber = phDto.getPhoneNumber();
                        String std = phoneNumber.substring(0, phoneNumber.indexOf("-"));
                        String landline = phoneNumber.substring((phoneNumber.indexOf("-") + 1), phoneNumber.length());
                        user.setStdCode(std);
                        user.setLandline(landline);
                        continue;
                    } else if ("Mobile".equals(phDto.getPhoneType())) {
                        String phoneNumber = phDto.getPhoneNumber();
                        user.setMobile(phoneNumber);
                    }
                }
            }
            RoleDTO role = dto.getRole();
            if (null != role) {
                user.setRole(role.getRoleName());
            }
            user.setUserId(dto.getId());
            users.add(user);
        }
        return users;
    }

}
