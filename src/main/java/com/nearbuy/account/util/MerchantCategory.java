package com.nearbuy.account.util;

import java.util.HashMap;
import java.util.Map;

public enum MerchantCategory {

    FOOD_BEVERAGES("Food & Beverages", "FNB"), SPA_SALON("Spa & Salon", "SNS"),

    HEALTH_FITNESS("Health & Fitness", "HNF"), HOME_AUTO_SERVICES("Home & Auto Services", "LOS"),

    GETAWAYS("Travel", "GTW"), THINGS_TO_DO("Things To Do", "TTD"), MOVIE_EVENT("Movies & Events", "MVE"), LOCAL_RETAIL("Local Shopping", "LOR");

    private static Map<String, String> categoryMapping = new HashMap<>();

    static {
        categoryMapping.put(FOOD_BEVERAGES.value, FOOD_BEVERAGES.display);
        categoryMapping.put(SPA_SALON.value, SPA_SALON.display);
        categoryMapping.put(HEALTH_FITNESS.value, HEALTH_FITNESS.display);
        categoryMapping.put(HOME_AUTO_SERVICES.value, HOME_AUTO_SERVICES.display);
        categoryMapping.put(GETAWAYS.value, GETAWAYS.display);
        categoryMapping.put(THINGS_TO_DO.value, THINGS_TO_DO.display);
        categoryMapping.put(MOVIE_EVENT.value, MOVIE_EVENT.display);
    }

    public final String display;

    public final String value;

    private MerchantCategory(String display, String value) {
        this.display = display;
        this.value = value;
    }

    public static String getMerchantCategoryDisplay(String value) {
        return categoryMapping.get(value);
    }

}
