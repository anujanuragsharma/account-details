package com.nearbuy.account.response.vo;

public class SocialNetworkLink {

    private SocialNetworkType socialNetworkType;

    private String url;

    public SocialNetworkType getSocialNetworkType() {
        return socialNetworkType;
    }

    public void setSocialNetworkType(SocialNetworkType socialNetworkType) {
        this.socialNetworkType = socialNetworkType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "SocialNetworkLink [socialNetworkType=" + socialNetworkType + ", url=" + url + "]";
    }

}
