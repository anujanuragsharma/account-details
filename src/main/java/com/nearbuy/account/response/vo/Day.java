package com.nearbuy.account.response.vo;

import java.util.HashMap;
import java.util.Map;

public enum Day {

    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday;

    static private Map<Integer, Day> dayMap = new HashMap<Integer, Day>();

    static {
        dayMap.put(1, Monday);
        dayMap.put(2, Tuesday);
        dayMap.put(3, Wednesday);
        dayMap.put(4, Thursday);
        dayMap.put(5, Friday);
        dayMap.put(6, Saturday);
        dayMap.put(7, Sunday);
    }

    public static Day getDay(int i) {
        return dayMap.get(i);
    }

}
