package com.nearbuy.account.response.vo;

import java.util.List;

public class MerchantDetailsVO {

    /*
     * 
     * name
     * logo
     * id
     * chainId
     * chainName
     * accountId
     * accountName
     * merchantType
     * Associated Sales Rep
     * aboutMerchant
     * usp
     * nickName
     * primaryCat
     * secondaryCat
     * redemptionAddress
     * Timings - Day, openTime, closeTime
     * Followers
     * Tags
     * Rating
     * contactInfo - phone number, emailId, reservation number, reservation Email Id, website, social n/w links
     *
     * 
     * 
     */

    private String name;

    private String logoUrl;

    private Long id;

    private String chainId;

    private String chainName;

    private String accountId;

    private String merchantType;

    private String aboutMerchant;

    private String usp;

    private String nickName;

    private String primaryCat;

    private String secondaryCat;

    private String redemptionAddress;

    private List<TimingVO> timings;

    private String followers;

    private List<String> tags;

    private String rating;

    private ContactInfoVO contactInfo;

    private List<AccountUserVO> accountUsers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getAboutMerchant() {
        return aboutMerchant;
    }

    public void setAboutMerchant(String aboutMerchant) {
        this.aboutMerchant = aboutMerchant;
    }

    public String getUsp() {
        return usp;
    }

    public void setUsp(String usp) {
        this.usp = usp;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPrimaryCat() {
        return primaryCat;
    }

    public void setPrimaryCat(String primaryCat) {
        this.primaryCat = primaryCat;
    }

    public String getSecondaryCat() {
        return secondaryCat;
    }

    public void setSecondaryCat(String secondaryCat) {
        this.secondaryCat = secondaryCat;
    }

    public String getRedemptionAddress() {
        return redemptionAddress;
    }

    public void setRedemptionAddress(String redemptionAddress) {
        this.redemptionAddress = redemptionAddress;
    }

    public List<TimingVO> getTimings() {
        return timings;
    }

    public void setTimings(List<TimingVO> timings) {
        this.timings = timings;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public ContactInfoVO getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfoVO contactInfo) {
        this.contactInfo = contactInfo;
    }

    public List<AccountUserVO> getAccountUsers() {
        return accountUsers;
    }

    public void setAccountUsers(List<AccountUserVO> accountUsers) {
        this.accountUsers = accountUsers;
    }

    @Override
    public String toString() {
        return "MerchantDetailsVO [name=" + name + ", logoUrl=" + logoUrl + ", id=" + id + ", chainId=" + chainId + ", chainName=" + chainName + ", accountId="
                + accountId + ", merchantType=" + merchantType + ", aboutMerchant=" + aboutMerchant + ", usp=" + usp + ", nickName=" + nickName
                + ", primaryCat=" + primaryCat + ", secondaryCat=" + secondaryCat + ", redemptionAddress=" + redemptionAddress + ", timings=" + timings
                + ", followers=" + followers + ", tags=" + tags + ", rating=" + rating + ", contactInfo=" + contactInfo + ", accountUsers=" + accountUsers
                + "]";
    }

}
