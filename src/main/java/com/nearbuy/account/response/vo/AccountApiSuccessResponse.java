package com.nearbuy.account.response.vo;

public class AccountApiSuccessResponse {

    public AccountApiSuccessResponse() {
    }

    public AccountApiSuccessResponse(String msg, Long id) {
        this.msg = msg;
        this.id = id;
    }

    private String msg;

    private Long id;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ApiResponse [msg=" + msg + ", id=" + id + "]";
    }

}
