package com.nearbuy.account.response.vo;

import java.util.List;

public class ChainDetailsVO {

    /*
     * 
     * 
     * chainId
     * chainName
     * companyLogo
     * associatedSalesRep
     * brandBannerImg
     * primaryCat
     * secondaryCat
     * chainImgs
     * businessAddress
     * phoneNumber
     * emailId
     * aboutMerchant
     * 
     * 
     * 
     * 
     */

    private Long id;

    private String chainName;

    private String logoUrl;

    private List<String> associatedSalesRep;

    private String brandBannerImg;

    private String primaryCat;

    private String secondaryCat;

    private List<String> chainImgs;

    private String aboutMerchant;

    private String businessAddress;

    private PhoneNumbers phoneNumbers;

    private List<String> emailIds;

    private List<AssociatedMerchantDetail> associatedMerchants;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public List<String> getAssociatedSalesRep() {
        return associatedSalesRep;
    }

    public void setAssociatedSalesRep(List<String> associatedSalesRep) {
        this.associatedSalesRep = associatedSalesRep;
    }

    public String getBrandBannerImg() {
        return brandBannerImg;
    }

    public void setBrandBannerImg(String brandBannerImg) {
        this.brandBannerImg = brandBannerImg;
    }

    public String getPrimaryCat() {
        return primaryCat;
    }

    public void setPrimaryCat(String primaryCat) {
        this.primaryCat = primaryCat;
    }

    public String getSecondaryCat() {
        return secondaryCat;
    }

    public void setSecondaryCat(String secondaryCat) {
        this.secondaryCat = secondaryCat;
    }

    public List<String> getChainImgs() {
        return chainImgs;
    }

    public void setChainImgs(List<String> chainImgs) {
        this.chainImgs = chainImgs;
    }

    public String getAboutMerchant() {
        return aboutMerchant;
    }

    public void setAboutMerchant(String aboutMerchant) {
        this.aboutMerchant = aboutMerchant;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public PhoneNumbers getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(PhoneNumbers phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<String> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(List<String> emailIds) {
        this.emailIds = emailIds;
    }

    public List<AssociatedMerchantDetail> getAssociatedMerchants() {
        return associatedMerchants;
    }

    public void setAssociatedMerchants(List<AssociatedMerchantDetail> associatedMerchants) {
        this.associatedMerchants = associatedMerchants;
    }

    @Override
    public String toString() {
        return "ChainDetailsVO [id=" + id + ", chainName=" + chainName + ", logoUrl=" + logoUrl + ", associatedSalesRep=" + associatedSalesRep
                + ", brandBannerImg=" + brandBannerImg + ", primaryCat=" + primaryCat + ", secondaryCat=" + secondaryCat + ", chainImgs=" + chainImgs
                + ", aboutMerchant=" + aboutMerchant + ", businessAddress=" + businessAddress + ", phoneNumbers=" + phoneNumbers + ", emailIds=" + emailIds
                + ", associatedMerchants=" + associatedMerchants + "]";
    }

}
