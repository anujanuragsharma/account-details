package com.nearbuy.account.response.vo;

import java.util.List;

public class ContactInfoVO {

    private PhoneNumbers phoneNumbers;

    private List<String> emailId;

    private PhoneNumbers reservationNumber;

    private List<String> reservationEmailId;

    private String website;

    private List<SocialNetworkLink> socialNetworkLinks;

    public PhoneNumbers getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(PhoneNumbers phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public PhoneNumbers getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(PhoneNumbers reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public List<String> getEmailId() {
        return emailId;
    }

    public void setEmailId(List<String> emailId) {
        this.emailId = emailId;
    }

    public List<String> getReservationEmailId() {
        return reservationEmailId;
    }

    public void setReservationEmailId(List<String> reservationEmailId) {
        this.reservationEmailId = reservationEmailId;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<SocialNetworkLink> getSocialNetworkLinks() {
        return socialNetworkLinks;
    }

    public void setSocialNetworkLinks(List<SocialNetworkLink> socialNetworkLinks) {
        this.socialNetworkLinks = socialNetworkLinks;
    }

    @Override
    public String toString() {
        return "ContactInfoVO [phoneNumbers=" + phoneNumbers + ", emailId=" + emailId + ", reservationNumber=" + reservationNumber + ", reservationEmailId="
                + reservationEmailId + ", website=" + website + ", socialNetworkLinks=" + socialNetworkLinks + "]";
    }

}
