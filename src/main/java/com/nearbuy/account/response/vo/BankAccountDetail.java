package com.nearbuy.account.response.vo;

public class BankAccountDetail {

    /*
     * BankDetail - name,ifsc, a/c number, A/c holder's name, nickname,
     */
    // Represents bankAcctId in merchant-profile
    private Long bankAcctId;

    private String name;

    private String ifsc;

    private String acNumber;

    private String acHolderName;

    private String nickname;
    
    private String chequeS3Url;
    
    private String chequeName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getAcNumber() {
        return acNumber;
    }

    public void setAcNumber(String acNumber) {
        this.acNumber = acNumber;
    }

    public String getAcHolderName() {
        return acHolderName;
    }

    public void setAcHolderName(String acHolderName) {
        this.acHolderName = acHolderName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getBankAcctId() {
        return bankAcctId;
    }

    public void setBankAcctId(Long bankAcctId) {
        this.bankAcctId = bankAcctId;
    }

    public String getChequeS3Url() {
        return chequeS3Url;
    }

    public void setChequeS3Url(String chequeS3Url) {
        this.chequeS3Url = chequeS3Url;
    }

    public String getChequeName() {
        return chequeName;
    }

    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BankAccountDetail [bankAcctId=");
        builder.append(bankAcctId);
        builder.append(", name=");
        builder.append(name);
        builder.append(", ifsc=");
        builder.append(ifsc);
        builder.append(", acNumber=");
        builder.append(acNumber);
        builder.append(", acHolderName=");
        builder.append(acHolderName);
        builder.append(", nickname=");
        builder.append(nickname);
        builder.append(", chequeS3Url=");
        builder.append(chequeS3Url);
        builder.append(", chequeName=");
        builder.append(chequeName);
        builder.append("]");
        return builder.toString();
    }

    
    
}
