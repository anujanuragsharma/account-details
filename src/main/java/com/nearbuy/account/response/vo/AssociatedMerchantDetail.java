package com.nearbuy.account.response.vo;

import java.util.List;

public class AssociatedMerchantDetail {

    /*
     * phonenumber, name, address
     */

    private Long merchantId;

    private List<String> phoneNumbers;

    private String name;

    private String address;

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        return "AssociatedMerchantDetail [merchantId=" + merchantId + ", phoneNumbers=" + phoneNumbers + ", name=" + name + ", address=" + address + "]";
    }

}
