//package com.nearbuy.account.response.vo;
//
//import java.util.List;
//
//public class UserSearchResponse {
//
//    private String username;
//
//    private List<String> phoneNumbers;
//
//    private String emailId;
//
//    private String acId;
//
//    private Long id;
//
//    public String getAcId() {
//        return acId;
//    }
//
//    public void setAcId(String acId) {
//        this.acId = acId;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public List<String> getPhoneNumbers() {
//        return phoneNumbers;
//    }
//
//    public void setPhoneNumbers(List<String> phoneNumber) {
//        this.phoneNumbers = phoneNumber;
//    }
//
//    public String getEmailId() {
//        return emailId;
//    }
//
//    public void setEmailId(String emailId) {
//        this.emailId = emailId;
//    }
//
//    @Override
//    public String toString() {
//        return "UserSearchResponse [username=" + username + ", phoneNumbers=" + phoneNumbers + ", emailId=" + emailId + ", acId=" + acId + ", id=" + id + "]";
//    }
//
//}
