package com.nearbuy.account.response.vo;

import java.util.List;

import com.nearbuy.account.model.MerchantPhoneNumber;

public class PhoneNumbers {

    private List<MerchantPhoneNumber> mobile;

    private List<MerchantPhoneNumber> landline;

    private List<MerchantPhoneNumber> tollfree;

    public List<MerchantPhoneNumber> getMobile() {
        return mobile;
    }

    public void setMobile(List<MerchantPhoneNumber> mobile) {
        this.mobile = mobile;
    }

    public List<MerchantPhoneNumber> getLandline() {
        return landline;
    }

    public void setLandline(List<MerchantPhoneNumber> landline) {
        this.landline = landline;
    }

    public List<MerchantPhoneNumber> getTollfree() {
        return tollfree;
    }

    public void setTollfree(List<MerchantPhoneNumber> tollfree) {
        this.tollfree = tollfree;
    }

    @Override
    public String toString() {
        return "PhoneNumbers [mobile=" + mobile + ", landline=" + landline + ", tollfree=" + tollfree + "]";
    }

}
