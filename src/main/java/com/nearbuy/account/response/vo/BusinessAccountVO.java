package com.nearbuy.account.response.vo;

import java.util.List;

public class BusinessAccountVO {

    /*
     * 
     * id
     * acct Name
     * legal name
     * business address
     * associated sales rep
     * List<AccountUser>
     * List<BankDetail>
     * List<MerchantDetails>
     * MerchantDetail - phonenumber, name, address
     * 
     */

    private Long id;

    private String name;

    private String legalName;

    private String address;

    private List<String> associatedSalesRep;

    private List<BankAccountDetail> bankDetails;

    private List<AssociatedMerchantDetail> merchants;

    private List<AccountUserVO> accountUsers;

    private String phoneNumber;

    private String emailAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getAssociatedSalesRep() {
        return associatedSalesRep;
    }

    public void setAssociatedSalesRep(List<String> associatedSalesRep) {
        this.associatedSalesRep = associatedSalesRep;
    }

    public List<BankAccountDetail> getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(List<BankAccountDetail> bankDetails) {
        this.bankDetails = bankDetails;
    }

    public List<AssociatedMerchantDetail> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<AssociatedMerchantDetail> merchants) {
        this.merchants = merchants;
    }

    public List<AccountUserVO> getAccountUsers() {
        return accountUsers;
    }

    public void setAccountUsers(List<AccountUserVO> accountUsers) {
        this.accountUsers = accountUsers;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BusinessAccountVO [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", legalName=");
        builder.append(legalName);
        builder.append(", address=");
        builder.append(address);
        builder.append(", associatedSalesRep=");
        builder.append(associatedSalesRep);
        builder.append(", bankDetails=");
        builder.append(bankDetails);
        builder.append(", merchants=");
        builder.append(merchants);
        builder.append(", accountUsers=");
        builder.append(accountUsers);
        builder.append(", phoneNumber=");
        builder.append(phoneNumber);
        builder.append(", emailAddress=");
        builder.append(emailAddress);
        builder.append("]");
        return builder.toString();
    }

}
