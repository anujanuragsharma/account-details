package com.nearbuy.account.response.vo;

public class AccountApiErrorResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccountApiErrorResponse [message=");
        builder.append(message);
        builder.append("]");
        return builder.toString();
    }

}
