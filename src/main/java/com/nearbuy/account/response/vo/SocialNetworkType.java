package com.nearbuy.account.response.vo;

import java.util.HashMap;
import java.util.Map;

public enum SocialNetworkType {

    FACEBOOK, TWITTER, GOOGLE_PLUS, PINTEREST, LINKEDIN, YOUTUBE, INSTAGRAM;

    static private Map<String, SocialNetworkType> map = new HashMap<String, SocialNetworkType>();

    static {
        map.put("fb", FACEBOOK);
        map.put("tw", TWITTER);
        map.put("g+", GOOGLE_PLUS);
        map.put("pinterest", PINTEREST);
        map.put("linkedin", LINKEDIN);
        map.put("yo", YOUTUBE);
        map.put("instagram", INSTAGRAM);
    }

    public static SocialNetworkType getSocialNetworkType(String type) {
        return map.get(type);
    }

}
