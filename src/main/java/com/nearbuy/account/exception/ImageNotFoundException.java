package com.nearbuy.account.exception;

public class ImageNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ImageNotFoundException() {
        // TODO Auto-generated constructor stub
    }

    public ImageNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public ImageNotFoundException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ImageNotFoundException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
