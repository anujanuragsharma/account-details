package com.nearbuy.account.exception;

public class UploadException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UploadException() {
    }

    public UploadException(String message, Exception cause) {
        super(message, cause);
    }

    public UploadException(String message) {
        super(message);
    }

    public UploadException(Exception cause) {
        super(cause);
    }

}
