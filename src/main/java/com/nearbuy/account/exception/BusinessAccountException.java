package com.nearbuy.account.exception;

public class BusinessAccountException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public BusinessAccountException(String message) {
        super(message);
    }

    public BusinessAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessAccountException(Throwable cause) {
        super(cause);
    }

}
