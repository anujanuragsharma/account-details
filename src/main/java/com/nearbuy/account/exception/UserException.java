package com.nearbuy.account.exception;

public class UserException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7382702684801208770L;

    public UserException() {

    }

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserException(Throwable cause) {
        super(cause);
    }
}
