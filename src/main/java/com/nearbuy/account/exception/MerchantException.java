package com.nearbuy.account.exception;

public class MerchantException extends Exception {

    private static final long serialVersionUID = 1L;

    public MerchantException(String message) {
        super(message);
    }

    public MerchantException(String message, Throwable cause) {
        super(message, cause);
    }

    public MerchantException(Throwable cause) {
        super(cause);
    }

}
